<?php
/*
header( 'Expires: Wed, 11 Jan 1984 05:00:00 GMT' );
header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
header( 'Cache-Control: no-cache, must-revalidate, max-age=0' );
header( 'Pragma: no-cache' );
*/
//兼容 app 视频列表
$host = strstr($_SERVER['HTTP_HOST'],'moredoo')? 'rest.api.moredoo.com' : 'rest.api.yyport.com';
$get = '?'.http_build_query($_GET);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://{$host}/v1/videos/applist".$get);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
$output = curl_exec($ch);
curl_close($ch);

if((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')){
	$output = str_replace('http:','https:',$output);
}

echo $output;