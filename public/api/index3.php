<?php
header( 'Expires: Wed, 11 Jan 1984 05:00:00 GMT' );
header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
header( 'Cache-Control: no-cache, must-revalidate, max-age=0' );
header( 'Pragma: no-cache' );
$list = array(
	array('img'=>'http://static.moredoo.com/storages/201606/a5b6c94d03cde9440fd1280e8554f37a/cover.jpg','video'=>'http://vod.moredoo.com/u/7575/mp4/0x0/ccd9a20fb4ef1ad5788563be0a94eff3.mp4','title'=>'中粮高清','desc'=>'中粮高清','live'=>0),
	array('img'=>'http://static.moredoo.com/storages/201606/67bdbbea15df5da1881bc70c54fc8264/cover.jpg','video'=>'http://vod.moredoo.com/u/7575/mp4/0x0/4c22090b10b41bc18997cea5aaf81656.mp4','title'=>'中粮标清','desc'=>'中粮标清','live'=>0),
	array('img'=>'http://static.moredoo.com/storages/201606/8601e1f34951c1b9d9eeb04b8ed5c8bd/cover.jpg','video'=>'http://vod.moredoo.com/u/7575/mp4/0x0/334c258c67e8cfa8ac3dd399bcc15c64.mp4','title'=>'娃哈哈高清','desc'=>'娃哈哈高清','live'=>0),
	array('img'=>'http://static.moredoo.com/storages/201606/0f833b3bf1be9621153832f33443cc21/cover.jpg','video'=>'http://vod.moredoo.com/u/7575/mp4/0x0/65b5c39988455dc297ada8a12b80222e.mp4','title'=>'娃哈哈标清','desc'=>'娃哈哈标清','live'=>0),
	array('img'=>'http://static.moredoo.com/storages/201606/e2baa10cd87e4f8214d2d4124c074439/cover.jpg','video'=>'http://vod.moredoo.com/u/7575/mp4/0x0/f9885e68a37b5fbd1d07125bb58a9b1a.mp4','title'=>'雀巢高清','desc'=>'雀巢高清','live'=>0),
	array('img'=>'http://static.moredoo.com/storages/201606/d4d02abb9572d9f443dd0bbfa1f35d03/cover.jpg','video'=>'http://vod.moredoo.com/u/7575/mp4/0x0/1268160c142446dc13a7b42b51810bfe.mp4','title'=>'雀巢标清','desc'=>'雀巢标清','live'=>0),
	array('img'=>'http://static.moredoo.com/storages/201606/ac10e5378dcaaa096a0ffbb00845d91e/cover.jpg','video'=>'http://vod.moredoo.com/u/7575/mp4/0x0/f63dc7b861660620b815e290551cd942.mp4','title'=>'康师傅高清','desc'=>'康师傅高清','live'=>0),
	array('img'=>'http://static.moredoo.com/storages/201606/1fb29841caa36f21b92f9d0cd84dd2ad/cover.jpg','video'=>'http://vod.moredoo.com/u/7575/mp4/0x0/8e0619309bf7fb7bf8ead1a07b3a3f33.mp4','title'=>'康师傅标清','desc'=>'康师傅标清','live'=>0),
);

require 'Page.php';
$show = empty($_GET['page']) || $_GET['page'] < 1? 1 : intval($_GET['page']);
$page = new Page(count($list),$show,12);
$limit = $page->getLimit();
$lists = array_slice($list,$limit[0],$limit[1]);

$back = $page->getBack();
$back[] = $show;
$pagelist = array_merge($back,$page->getGoing());
$a = array(
	'code'=>200,
	'page'=>array('current'=>$page->getShow(),'prev'=>$page->getPrev(),'next'=>$page->getNext(),'list'=>$pagelist),
	'list'=> $lists
);

echo json_encode($a);
