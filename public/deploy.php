<?php
/**
* 自动部署脚本
*************************************
* 将该文件放到 public 目录
* 生成 deployKey
*************************************
* 要求：
* 1. 必须设置一个备份目录
* 2. 必须为 git 配置 public key
* 3. 必须安装 composer
*************************************
*/
set_time_limit(0);
$deployKey = '1234567'; //部署密钥
$projectDir = dirname(dirname(__FILE__)); //项目目录
$baseDir = dirname(dirname(dirname(__FILE__))); //根目录
$backupDir = $baseDir.'/backup'; //备份目录
$projectName = basename($projectDir);

function response($msg,$code=1){
	echo json_encode(array('code'=>$code,'data'=>$msg));
	exit;
}

if(empty($_REQUEST['v'])){
	$_REQUEST['v'] = '';
}

$verify = md5($deployKey.$_REQUEST['m'].$_REQUEST['v']);
if($verify != $_REQUEST['key']){
	response('key invalid',0);
}

if(!is_dir($backupDir)){
	response('备份文件夹不存在',0);
}

switch($_REQUEST['m']){
	case 'backup': //备份
		if(empty($_REQUEST['v'])){
			response('备份日期不正确',0);
		}
		$backup = "{$backupDir}/{$projectName}_{$_REQUEST['v']}";
		if(is_dir($backup)){
			response('备份已存在',0);
		}
		chdir($projectDir);
		exec("mkdir {$backup}");
		exec("sudo \cp -rf `ls | grep -v .git | xargs` {$backup}",$output,$code);
		if($code){
			response($output,0);
		}else{
			response("backup：{$backup}");
		}
		break;
	case 'showbackup': //显示备份
		$folds = glob($backupDir."/*",GLOB_ONLYDIR);
		foreach($folds as $k=>$item){
			$folds[$k] = basename($item);
		}
		response($folds);
		break;
	case 'rollback': //回滚
		$backup = "{$backupDir}/{$_REQUEST['v']}";
		if(!is_dir($backup)){
			response('备份不存在',0);
		}
		chdir($backup);
		exec("sudo \cp -rf `ls | grep -v .git | xargs` {$projectDir}",$output,$code);
		if($code){
			response($output,0);
		}else{
			response('回滚完成');
		}
		break;
	case 'update': //升级
		chdir($projectDir);
		exec("git fetch --all");
		exec("git reset --hard origin/master");
		exec("rm -rf src/settings.php");
		exec("mv src/settings_release.php src/settings.php");
		exec("composer dump-autoload");
		response('升级完成');
		break;
}
