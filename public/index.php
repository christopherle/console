<?php
// $date = '2017-01-04';
// $w = (int)date('w',strtotime($date))-1;
// echo $w;
// echo date("Y-m-d H:i:s",strtotime($date)-$w*86400);
// exit;
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    if (is_file($file)) {
        return false;
    }
}
// error_reporting(0);

//=======================================================获取HTTP请求头信息
/*
function getHeadersString(){
	$header = getallheaders();
	$string = '';
	foreach ($header as $name => $value) {
		$string .= "{$name}: {$value}\r\n";
	}
	$string .= "URL: http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."\r\n";
	$string .= "POST: ".urldecode(http_build_query($_REQUEST))."\r\n";
	$string .= "GLOB: ".urldecode(http_build_query($GLOBALS))."\r\n";
	return $string;
}

$data = http_build_query(['type'=>'log','error'=>getHeadersString()]);
$opts = array(
	'http'=>array(
	 'method'=>"POST",
	 'header'=>"Content-type: application/x-www-form-urlencoded\r\n".
			   "Content-length:".strlen($data)."\r\n" .
			   "\r\n",
	 'content' => $data,
	)
);
$cxContext = stream_context_create($opts);
$sFile = file_get_contents('http://rest.api.moredoo.com/v1/logs/add', false, $cxContext);
*/
//-=================================================================

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
