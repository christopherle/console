function getQuery(name) {
	var query = [];
	var arr = window.location.search.replace('?','').split('&');
	for(var i in arr){
		var item = arr[i].split('=');
		query[item[0]] = item[1];
	}
	if(!name) return query;
	if(!query[name]) return null;
	return decodeURI(query[name]);
}

$('.PageTool a').click(function(){
	var page = $(this).attr('data-page');
	if(!page) return;
	var query = getQuery();
	query['page'] = page;
	var url = [];
	for(var i in query){
		url.push(i+'='+query[i]);
	}
	window.location.href = '?'+url.join('&');
});