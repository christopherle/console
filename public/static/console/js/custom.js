/* custom */
// var Logo={
// 		uplogo_ratio:<?= $post['uplogo_ratio'] ?>*100+'%',
// 		downlogo_ratio:<?= $post['downlogo_ratio'] ?>*100+'%',
// 		uplogo_rotate:<?= $post['uplogo_rotate'] ?>,
// 		downlogo_rotate:<?= $post['downlogo_rotate'] ?>
// 	};
// var Custom=(function(){
// 	/* logo */
// 	var logoCustom={
// 		/* 要用到的选择器 */
// 		selector:{
// 			uplogo_div:$(".up-logo div"),
// 			downlogo_div:$(".down-logo div"),
// 			up_rotate_time:$(".up-rotate-time"),
// 			down_rotate_time:$(".down-rotate-time"),
// 			down_ratio:$("#down-ratio"),
// 			down_rotate:$("#down-rotate"),
// 			up_ratio:$("#up-ratio"),
// 			up_rotate:$("#up-rotate"),
// 		},
// 		/* logo初始化 */
// 		render:function(){
// 			/* 初始化logo设置大小 */
// 			logoCustom.selector.uplogo_div.css('backgroundSize',Logo.uplogo_ratio);
// 			logoCustom.selector.downlogo_div.css('backgroundSize',Logo.downlogo_ratio);
// 			/* 初始化logo旋转方向和速度 */
// 			if(Logo.uplogo_rotate<0){
// 				logoCustom.selector.up_rotate_time.removeClass('hover-clockwise').addClass('hover-anticlockwise');
//  				logoCustom.selector.up_rotate_time.css('animationDuration',-360/Logo.uplogo_rotate+'s');
// 			}else{
// 				logoCustom.selector.up_rotate_time.removeClass('hover-anticlockwise').addClass('hover-clockwise');
//  				logoCustom.selector.up_rotate_time.css('animationDuration',360/Logo.uplogo_rotate+'s');
// 			}
// 			if(Logo.downlogo_rotate<0){
// 				logoCustom.selector.down_rotate_time.removeClass('hover-clockwise').addClass('hover-anticlockwise');
//  				logoCustom.selector.down_rotate_time.css('animationDuration',-360/Logo.downlogo_rotate+'s');
// 			}else{
// 				logoCustom.selector.down_rotate_time.removeClass('hover-anticlockwise').addClass('hover-clockwise');
//  				logoCustom.selector.down_rotate_time.css('animationDuration',360/Logo.downlogo_rotate+'s');
// 			}
// 			/* 控制条滑动 */
// 			logoCustom.selector.down_ratio.slider();
// 		},
// 		/* 设置logo大小、旋转速度、旋转方向 */
// 		setLogo:{
			
// 		}
// 	};
// 	logoCustom.render();
// })();

//logo大小显示为设置值
function logoRatio(){
	$('.down-logo div').css('backgroundSize',<?= $post['downlogo_ratio'] ?>*100+'%');
	$('.up-logo div').css('backgroundSize',<?= $post['uplogo_ratio'] ?>*100+'%');
}
logoRatio();

//logo旋转为默认值
function logoRotate(){
	var step_up=<?= $post['uplogo_rotate'] ?>;
	var step_down=<?= $post['downlogo_rotate'] ?>;
	var speed_up=360/step_up;
	var speed_down=360/step_down;
	if(step_up<0){
		$('.up-rotate-time').removeClass('hover-clockwise').addClass('hover-anticlockwise');
		$('.up-rotate-time').css('animationDuration',-speed_up+'s');
	}else{
		$('.up-rotate-time').removeClass('hover-anticlockwise').addClass('hover-clockwise');
		$('.up-rotate-time').css('animationDuration',speed_up+'s');
	}
	if(step_down<0){
		$('.down-rotate-time').removeClass('hover-clockwise').addClass('hover-anticlockwise');
		$('.down-rotate-time').css('animationDuration',-speed_down+'s');
	}else{
		$('.down-rotate-time').removeClass('hover-anticlockwise').addClass('hover-clockwise');
		$('.down-rotate-time').css('animationDuration',speed_down+'s');
	}
}
logoRotate();

//底部logo大小，旋转设置
$("#down-ratio").slider();
$("#down-ratio").on("change", function(slideEvt) {
	var newWidth=slideEvt.value.newValue*100+'%';
	$('.down-logo div').css('backgroundSize',newWidth);
});
$("#down-rotate").slider();
$("#down-rotate").on("change", function(slideEvt) {
	var time=360/slideEvt.value.newValue;
	if(time<0){
		$('.down-rotate-time').removeClass('hover-clockwise').addClass('hover-anticlockwise');
		$('.down-rotate-time').css('animationDuration',-time+'s');
	}else{
		$('.down-rotate-time').removeClass('hover-anticlockwise').addClass('hover-clockwise');
		$('.down-rotate-time').css('animationDuration',time+'s');
	}
});

//顶部logo大小，旋转设置
$("#up-ratio").slider();
$("#up-ratio").on("change", function(slideEvt) {	
	var newWidth=slideEvt.value.newValue*100+'%';
	$('.up-logo div').css('backgroundSize',newWidth);
});
$("#up-rotate").slider();
$("#up-rotate").on("change", function(slideEvt) {
	var time=360/slideEvt.value.newValue;
	if(time<0){
		$('.up-rotate-time').removeClass('hover-clockwise').addClass('hover-anticlockwise');
		$('.up-rotate-time').css('animationDuration',-time+'s');
	}else{
		$('.up-rotate-time').removeClass('hover-anticlockwise').addClass('hover-clockwise');
		$('.up-rotate-time').css('animationDuration',time+'s');
	}
});
//重置图片大小和logo旋转速度
$('.reset-ratio').click(function(){
	$(this).parent().siblings().find('input:first').slider('setValue',0.25,'true');
	$(this).parent().parent().parent().find('.showimg-logo div').css('backgroundSize','25%');
})
$('.reset-rotate').click(function(){
	$(this).parent().siblings().find('input:first').slider('setValue',0,'true');
	$(this).parent().parent().parent().find('.showimg-logo div').css('animationDuration',0+'s');
})

//选择自定义配置内容
$('.nav_2 li').click(function(){
	$(this).addClass('active').siblings().removeClass('active');
	var id=$(this).attr('data-id');
	if(id==1){
		$('.part1').siblings().slideUp('200',function(){
			$('.part1').slideDown('200');
			$('#sub').show();
		});
	}else if(id==2){
		$('.part2').siblings().slideUp('200',function(){
			$('.part2').slideDown('200');
			$('#sub').show();
		});
	}else if(id==3){
		$('.part3').siblings().slideUp('200',function(){
			$('.part3').slideDown('200');
			$('#sub').show();
		});
	}else if(id==4){
		$('.part4').siblings().slideUp('200',function(){
			$('.part4').slideDown('200');
			$('#sub').show();
		});
	}
})
//选择私密访问或者公开访问
$('.nav_3 li').click(function(){
	$(this).addClass('active').siblings().removeClass('active');
	var id=$(this).attr('data-id');
	if(id==1){
		$('.custom1').show();
		$('#codesub').val('');
		$('.private').hide();
	}else if(id==2){
		if($('#code').val()==''){
			randomCode();
		}
		$('.custom1').hide();
		$('.private').show();
	}
})
//上传图片
function upload(obj){
	var formdata = new FormData();
	formdata.append('file',$(obj)[0].files[0]);
	$(obj).attr('type','text');
	$(obj).attr('disabled','disabled');
	$(obj).val(' 上传中...');
	$.ajax({
		url: '<?= $uploadurl ?>',
		type: 'POST',
		cache: false,
		dataType:'JSON',
		data: formdata,
		processData: false,
		contentType: false
	}).done(function(res) {
		setTimeout(function(){
			if(res.url){
				$(obj).parent().find('input:first').val(res.url);
				$(obj).parent().parent().find('img:first').attr('src',res.url);
				$(obj).parent().siblings().find('div').css('background-image','url('+res.url+')');
				$(obj).removeAttr('disabled');
				$(obj).val('');
				$(obj).attr('type','file');
			}else{
				alert(res.error);
			}
		},500);
	}).fail(function(res) {
		console.log(res);
	});
}
//显示顶部logo设置内容
$('.uplogo_hide').click(function(){
	var statusUp=$(this).attr('data-status');
	if(statusUp==0){
		$('.hide-logo-up b').html('隐藏');
		$(this).parent().siblings().slideUp();
		$(this).attr('data-status','1');
	}else{
		$('.hide-logo-up b').html('显示');
		$(this).parent().siblings().slideDown();
		$(this).attr('data-status','0');
	}
})
//显示底部logo设置内容
$('.downlogo_hide').click(function(){
	var statusDown=$(this).attr('data-status');
	if(statusDown==0){
		$('.hide-logo-down b').html('隐藏');
		$(this).parent().siblings().slideUp();
		$(this).attr('data-status','1');
	}else{
		$('.hide-logo-down b').html('显示');
		$(this).parent().siblings().slideDown();
		$(this).attr('data-status','0');
	}
})
//随机私密房间密码
function randomCode(){
	var code=parseInt(Math.random()*10000);
	if(code<999){
		code=code+999;
	}
	$('#code').val(code);
	$('#codesub').val(code);	
}
$('.resetcode').on('click',function(){
	randomCode();
})