var textIn=function(Num){
	this.part1=function(){
		setTimeout(function(){
			TweenLite.from($("#h-1"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h-1"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},200);
		setTimeout(function(){
			TweenLite.from($("#h-2"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h-2"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},400);
		setTimeout(function(){
			TweenLite.from($("#h-3"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h-3"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},600);
		setTimeout(function(){
			TweenLite.from($("#h-4"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h-4"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},800);
		setTimeout(function(){
			TweenLite.from($("#h-5"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h-5"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},1000);
	}
	this.part2_1=function(){
		setTimeout(function(){
			$("#img-control").show();
			TweenLite.from($("#img-control"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#img-control"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},200);
		setTimeout(function(){
			TweenLite.from($("#h1"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h1"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},200);
		setTimeout(function(){
			TweenLite.from($("#h2"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h2"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},400);
		setTimeout(function(){
			TweenLite.from($("#h3"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h3"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},600);
		setTimeout(function(){
			TweenLite.from($("#h4"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h4"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},800);
		setTimeout(function(){
			TweenLite.from($("#h5"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h5"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},1000);
	}
	this.part2_2=function(){
		setTimeout(function(){
			$("#img-work").show();
			TweenLite.from($("#img-work"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#img-work"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},200);
		setTimeout(function(){
			TweenLite.from($("#h1-1"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h1-1"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},200);
		setTimeout(function(){
			TweenLite.from($("#h2-1"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h2-1"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},400);
		setTimeout(function(){
			TweenLite.from($("#h3-1"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h3-1"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},600);
		setTimeout(function(){
			TweenLite.from($("#h4-1"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h4-1"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},800);
		setTimeout(function(){
			TweenLite.from($("#h5-1"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h5-1"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},1000);
	}
	this.part3=function(){
		setTimeout(function(){
			$("#img-cloud").show();
			TweenLite.from($("#img-cloud"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#img-cloud"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},200);
		setTimeout(function(){
			TweenLite.from($("#h_1"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h_1"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},200);
		setTimeout(function(){
			TweenLite.from($("#h_2"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h_2"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},400);
		setTimeout(function(){
			TweenLite.from($("#h_3"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h_3"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},600);
		setTimeout(function(){
			TweenLite.from($("#h_4"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h_4"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},800);
		setTimeout(function(){
			TweenLite.from($("#h_5"), 1, {scale:0.0, opacity:0.0,display:'block'});
			TweenLite.to($("#h_5"), 1, {scale:1.0, opacity:1.0,display:'block'});	
		},1000);
	}

	//gif互动元素轮播
	this.loopPlay=function(Num){
		var img=document.getElementsByClassName('part');
		var li=document.getElementsByClassName('tab_1');
		var imgnum=img.length;
		var num=Num;
		var timer="";
		clearInterval(timer);
		imgarr_1=['url(../static/home/img/productimg/interaction/3_ico_1.png)','url(../static/home/img/productimg/interaction/3_ico_2.png)','url(../static/home/img/productimg/interaction//3_ico_3.png)','url(../static/home/img/productimg/interaction//3_ico_4.png)'];
		imgarr_2=['url(../static/home/img/productimg/interaction/3_ico_1.1.png)','url(../static/home/img/productimg/interaction/3_ico_2.1.png)','url(../static/home/img/productimg/interaction//3_ico_3.1.png)','url(../static/home/img/productimg/interaction//3_ico_4.1.png)'];
		function show(){
			if(num<4){
				$('.part-container .part').addClass('hidden');
				$('.part-container .part:eq('+num+')').removeClass('hidden');
				var pre=num-1;
				if(pre>=0){
					$('.tab .tab_1:eq('+pre+')').css('background-image',imgarr_2[num-1]);
				}else if(num==0){
					$('.tab .tab_1:eq(3)').css('background-image','url(../static/home/img/productimg/interaction/3_ico_4.1.png)');
				}
				$('.tab .tab_1:eq('+num+')').css('background-image',imgarr_1[num]);
				num++;
			}else if(num==4){			
				num=0;
				show();
			}
		}
		show();
		timer=setInterval(function(){
			show();
		},5500)
		$('.tab .tab_1').mouseover(function(){
			clearInterval(timer);
			$('.part-container .part').addClass('hidden');
			var index=$(this).attr('data-index');
			$('.part-container .part:eq('+index+')').removeClass('hidden');
			for(var i=0;i<4;i++){
				$('.tab .tab_1:eq('+i+')').css('background-image',imgarr_2[i]);
			}
			$('.tab .tab_1:eq('+index+')').css('background-image',imgarr_1[index]);
		})
	}
	//
	this.playerInfo=function(){
		$('#accordion li').bind({
			mouseover:function(){
				$(this).find('.circle').addClass('active-1');//.css('box-shadow','1px 0 16px 0 #f97600');
			},
			mouseout:function(){
				var none=$(this).find('.submenu').css('display');
				if(none=='none'){
					$(this).find('.circle').removeClass('active-1'); //.css('box-shadow','none');
				}
			},
			click:function(){
				var id=$(this).attr('data-id');
				if(id==1){
					$(this).parent().find('.circle:eq(1)').removeClass('active-1');
					$(this).find('.submenu:eq(1)').removeClass('active');
					$('#html5-player').fadeIn(1000);
					$('#ios-player').css('display','none');
				}else{
					$(this).parent().find('.circle:eq(0)').removeClass('active-1');
					$(this).find('.submenu:eq(0)').removeClass('active');
					$('#html5-player').css('display','none');
					$('#ios-player').fadeIn(1000);
				}
				$(this).find('.circle').addClass('active-1');
				$(this).find('.submenu').addClass('active');
			}
		})
		$(function() {
			var Accordion = function(el, multiple) {
				this.el = el || {};
				this.multiple = multiple || false;

				// Variables privadas
				var links = this.el.find('.link');
				// Evento
				links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
			}

			Accordion.prototype.dropdown = function(e) {
				var $el = e.data.el;
					$this = $(this),
					$next = $this.next();

				$next.slideToggle();
				$this.parent().toggleClass('open');

				if (!e.data.multiple) {
					$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
				};
			}	

			var accordion = new Accordion($('#accordion'), false);
		});
	}
}