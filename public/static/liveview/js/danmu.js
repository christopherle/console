/**
* 弹幕插件
* var danmu = this.fn.danmu();
* danmu.add('添加弹幕');
* danmu.on() //开启弹幕
* danmu.off() //关闭弹幕
* danmu.turn //弹幕开启关闭状态
*/
(function(plugs){
	//火狐浏览器 watch 是系统关键字
	plugs.fn.danmu = function(){
		//创建舞台
		var box = document.createElement('div');
		box.style.position = "relative";
		box.style.overflow = 'hidden';
		box.style.width = '100%';
		box.style.height = '100%';
		box.style.pointerEvents = 'none';
		box.style.userSelect = 'none';
		var stage = plugs.fn.player.getDiv();
		stage.insertBefore(box,stage.childNodes[0]);
		
		//默认参数
		var colors = ["#0066FF","#00CC66","#3300CC","#339999","#6600FF" ,"#CC00CC","#CCFF33","#FF9900","#FFFF33","#FFFFFF"];
		var types = ['top','bottom','roll'];
		var fontsizes = [26,22,18,14];
		var speeds = [4,3,2,2];
		var rows = Math.floor(box.offsetHeight / 36);
		var that = this;
		
		//弹幕行
		var stageRows = [];
		for(var i = 0; i< rows; ++i){
			stageRows[i] = [];
		}
		
		//随机数
		function randNumber(Min,Max){
			var Range = Max - Min;
			var Rand = Math.random();
			return(Min + Math.round(Rand * Range));
		}
		
		//判断队列是否为空
		function empty(arr){
			var isempty = true;
			for(var i in arr){
				if(arr[i].length > 0){
					isempty = false;
				}
			}
			return isempty;
		}
		
		//移动弹幕
		this.intervalQueue = null;
		function move(){
			var overWidth = box.offsetWidth*2;
			that.intervalQueue = setInterval(function(){
				for(var i in stageRows){
					if(stageRows[i].length > 0){
						for(var k in stageRows[i]){
							if(stageRows[i][k].count == 0){
								box.appendChild(stageRows[i][k].ele);
								stageRows[i][k].count += stageRows[i][k].speed;
							}else if(stageRows[i][k].count > overWidth){
								console.log('removed...');
								box.removeChild(stageRows[i][k].ele);
								stageRows[i].splice(k,1);
							}else{
								stageRows[i][k].count += stageRows[i][k].speed;
								var right = parseInt(stageRows[i][k].ele.style.right.replace('px',''));
								stageRows[i][k].ele.style.right = (right+stageRows[i][k].speed) + 'px';
							}
						}
					}
				}
				
				//无字幕清除计时器
				if(empty(stageRows)){
					clearInterval(that.intervalQueue);
					that.intervalQueue = null;
					console.log('清除计时器');
				}
			},20);
		}
		
		//加入队列
		this.insertToStageRows = function(text,color,size){
			if(!this.turn) return;
			var span = document.createElement('span');
			span.style.position = 'absolute';
			span.style.whiteSpace = 'nowrap';
			span.style.textShadow = '1px 1px 1px #000000';
			span.style.color = color;
			span.style.zIndex = '100';
			span.innerHTML = text;
			
			//选择速度与文字大小，文字少的快大，文字多的慢小
			var speed = speeds[0];
			size = fontsizes[0];
			if(text.length > 6){
				speed = speeds[1];
				size = fontsizes[3];
			}
			if(text.length > 12){
				speed = speeds[2];
				size = fontsizes[2];
			}
			if(text.length > 24){
				speed = speeds[3];
				size = fontsizes[1];
			}
			span.style.fontSize = size+'px';
			var spanwidth = text.length * size + 80;
			
			//寻找插入弹幕的空行
			var index = 0;
			for(var i = 0; i < stageRows.length; ++i){
				//空行可直接添加
				if(stageRows[i].length <= 0){
					index = i;
					break;
				//弹幕已经跑远了可再次添加
				}else if(stageRows[i].length > 0 && stageRows[i][stageRows[i].length-1].count > spanwidth){
					index = i;
					break;
				//全满了插入第一行
				}else{
					index = 0;
				}
			}
			
			//弹幕重叠加速
			if(stageRows[index].length > 0){
				speed+=1;
			}
			span.style.top = (index*32)+6+'px';
			span.style.right = -(spanwidth-100)+'px';
			
			stageRows[index].push({count:0,speed:speed,ele:span});
			
			//重新唤醒计时器
			if(this.intervalQueue === null){
				move();
				console.log('开启计时器');
			}
		}
		
		//添加弹幕
		this.add = function(msg,color,size){
			if(typeof(color) == 'undefined'){
				color = colors[randNumber(0,colors.length-1)];
			}
			if(typeof(size) == 'undefined'){
				size = fontsizes[randNumber(0,fontsizes.length-1)];
			}
			this.insertToStageRows(msg,color,size);
		}
		
		//关闭弹幕
		this.turn = true;
		this.off = function(){
			clearInterval(this.intervalQueue);
			box.innerHTML = '';
			this.intervalQueue = null;
			for(var i in stageRows){
				stageRows[i] = [];
			}
			this.turn = false;
			console.log('danmu off...');
			return 'off';
		}
		
		//开启弹幕
		this.on = function(){
			this.turn = true;
			console.log('danmu on...');
			return 'on';
		}
		
		return this;
	}
	
})(MDVRPlayer);