/*
//聊天视图
conf = {
	stage:'list',
	msg_info:{type:"user",nickname:"火星网友","msg":"聊天内容"},
	order:"asc",
	msgtpl:{
		user:"<div class=\"{type}\"><span>{nickname}：</span><b>{msg}</b></div>",
		sys:"<div class=\"{type}\"><span>{nickname}：</span><b>{msg}</b></div>"
		notice:"<div class=\"{type}\"><b>{msg}</b></div>",
	}
}
*/
var chatViewManager = function(conf){
	//默认配置
	if(typeof(conf.max_msg_num) == 'undefined'){
		conf.max_msg_num = 20;
	}
	
	//是否滚屏
	var autoScroll = true;
	
	this.setAutoScroll = function(is){
		autoScroll = !!is;
	}
	
	//格式化用户消息
	this.formatMsg = function(msg){
		var tpl = conf['msgtpl'][msg.type];
		for(var i in conf.msg_info){
			var preg = new RegExp('{'+i+'}');
			tpl = tpl.replace(preg, msg[i]);
		}
		return tpl;
	}
	
	//将消息添加到节点
	this.appendHistory = function(msg){
		var p = document.getElementById(conf.stage);
		var html = this.formatMsg(msg);
		if(conf.order == 'desc'){
			p.innerHTML += html;
		}else{
			p.innerHTML = html+p.innerHTML;
		}
	}

	//将消息添加到节点
	this.appendMsg = function(msg){
		var p = document.getElementById(conf.stage);
		var html = this.formatMsg(msg);
		if(conf.max_msg_num > 0 && p.childNodes.length > conf.max_msg_num){
			if(conf.order=='asc'){
				p.removeChild(p.firstElementChild);
			}else{
				p.removeChild(p.lastElementChild);
			}
		}
		if(conf.order == 'asc'){
			p.innerHTML += html;
			if(autoScroll) p.scrollTop = p.scrollHeight;
		}else{
			p.innerHTML = html+p.innerHTML;
			if(autoScroll) p.scrollTop = 0;
		}
	}
	
	//构造用户消息数据格式
	this.buildMsgInfo = function(type,text){
		var info = conf.msg_info;
		info.type = type;
		info.msg = text;
		return info;
	}

	//发送通知
	this.addNoticeMsg = function(text){
		this.appendMsg({type:"notice",nickname:'通知',msg:text});
	}
	
	//发送系统消息
	this.addSysMsg = function(text){
		this.appendMsg({type:"sys",nickname:'系统消息',msg:text});
	}
	
	//发送用户消息
	this.addUserMsg = function(text){
		console.log(conf);
		this.appendMsg({type:"user",nickname:conf.msg_info.nickname,msg:text});
	}
}

/*
//dms服务库地址
http://cdn.aodianyun.com/dms/rop_client.js
var conf = {
	pub_key:"pub_b841dc836e24073eb40de120e07467e7",
	sub_key:"sub_3243918eb925492163b08e8bb6998430",
	client_id:"userid",
	topic:"topicid"
}
*/
var chatDmsManager = function(conf,view){
	//重连次数
	this.reconnect = 0;
	var that = this;
	
	//连接服务器
	function connectDMS(){
		if(that.reconnect < 5){
			view.addNoticeMsg('欢迎进入本直播间...');
			//view.addNoticeMsg('连接聊天服务器...');
			ROP.Enter(conf.pub_key,conf.sub_key,conf.client_id);
			that.reconnect++;
		}else{
			view.addNoticeMsg('网络不给力，请刷新页面...');
		}
	}
	connectDMS();
	
	//连接成功
	ROP.On("enter_suc",function() {
		 view.addNoticeMsg('连接成功...');
		 ROP.Subscribe(conf.topic);
		 ROP.Subscribe('__present__'+conf.topic);
	});
	//连接失败
	ROP.On("enter_fail",function(err) {
		view.addNoticeMsg("连接失败:" + err);
	});
	//断开连接
	ROP.On("losed",function() {
		view.addNoticeMsg("连接已断开...");
	})
	//离线
	ROP.On("offline",function(err) {
		view.addNoticeMsg("网络不给力，正在尝试重连...");
		connectDMS();
	});
	
	//收到消息
	ROP.On("publish_data",function(data,topic){
		var msg = JSON.parse(data);
		if(typeof(msg.cmd) == 'undefined') view.appendMsg(msg);
		if(that.msg_callbacks.length > 0){
			for(var i in that.msg_callbacks){
				that.msg_callbacks[i](msg,topic);
			}
		}
	});
	
	//发布系统消息
	this.publishSys = function(text){
		ROP.Publish(JSON.stringify(view.buildMsgInfo('sys',text)),conf.topic);
	}
	
	//发布聊天
	this.publish = function(text){
		if(text == '') return;
		if(text.length > 32){
			alert('聊天内容最多32个字');return;
		}
		ROP.Publish(JSON.stringify(view.buildMsgInfo('user',text)),conf.topic);
	}
	
	//添加收到消息回调方法
	this.msg_callbacks = [];
	this.addCallback = function(obj){
		this.msg_callbacks.push(obj);
	}
}

/*
//聊天历史消息管理
var historyconf = {
	"server":"http://rest.api.moredoo.com",
	"topic":"topic"
}
*/
var dmsHistoryManager = function(conf){
	//获取历史消息
	this.get = function (start,cb){
		$.ajax({
			url:conf.server+'/v1/service/dms/history/'+conf.topic+'/'+start,
			dataType:'JSON',
			success:function(rsp){
				var msgs = [];
				if(rsp.length > 0){
					for(var i in rsp){
						var item = rsp[i];
						var msg = JSON.parse(item.msg);
						msg.id = item._id;
						msgs.push(msg);
					}
					cb && cb(msgs);
				}
			},
			error:function(err){
				console.log(err);
			}
		});
	}
	
	//删除历史
	this.del = function (id,cb){
		$.ajax({
			url:conf.server+'/v1/service/dms/history/'+conf.topic+'/'+id,
			dataType:'JSON',
			method:'DELETE',
			success:function(rsp){
				cb && cb(rsp);
			},
			error:function(err){
				console.log(err);
			}
		});
	}
}

var danmuViewManager = function(conf){
	//创建舞台
	var box = document.createElement('div');
	box.style.position = "relative";
	box.style.overflow = 'hidden';
	box.style.width = '100%';
	box.style.height = '100%';
	conf.stage.insertBefore(box,conf.stage.childNodes[0]);
	
	//默认参数
	var colors = ["#0066FF","#00CC66","#3300CC","#339999","#6600FF" ,"#CC00CC","#CCFF33","#FF9900","#FFFF33","#FFFFFF"];
	var types = ['top','bottom','roll'];
	var fontsizes = [26,22,18,14];
	var speeds = [4,3,2,2];
	var rows = Math.floor(box.offsetHeight / 36);
	var that = this;
	
	//弹幕行
	var stageRows = [];
	for(var i = 0; i< rows; ++i){
		stageRows[i] = [];
	}
	
	//随机数
	function randNumber(Min,Max){
		var Range = Max - Min;
		var Rand = Math.random();
		return(Min + Math.round(Rand * Range));
	}
	
	//判断队列是否为空
	function empty(arr){
		var isempty = true;
		for(var i in arr){
			if(arr[i].length > 0){
				isempty = false;
			}
		}
		return isempty;
	}
	
	//移动弹幕
	this.intervalQueue = null;
	function move(){
		var overWidth = box.offsetWidth*2;
		that.intervalQueue = setInterval(function(){
			for(var i in stageRows){
				if(stageRows[i].length > 0){
					for(var k in stageRows[i]){
						if(stageRows[i][k].count == 0){
							box.appendChild(stageRows[i][k].ele);
							stageRows[i][k].count += stageRows[i][k].speed;
						}else if(stageRows[i][k].count > overWidth){
							console.log('removed...');
							box.removeChild(stageRows[i][k].ele);
							stageRows[i].splice(k,1);
						}else{
							stageRows[i][k].count += stageRows[i][k].speed;
							var right = parseInt(stageRows[i][k].ele.style.right.replace('px',''));
							stageRows[i][k].ele.style.right = (right+stageRows[i][k].speed) + 'px';
						}
					}
				}
			}
			
			//无字幕清除计时器
			if(empty(stageRows)){
				clearInterval(that.intervalQueue);
				that.intervalQueue = null;
				console.log('清除计时器');
			}
		},20);
	}
	
	//加入队列
	this.insertToStageRows = function(text,color,size){
		if(!this.turn) return;
		var span = document.createElement('span');
		span.style.position = 'absolute';
		span.style.whiteSpace = 'nowrap';
		span.style.textShadow = '1px 1px 1px #000000';
		span.style.color = color;
		span.style.zIndex = '100';
		span.innerHTML = text;
		
		//选择速度与文字大小，文字少的快大，文字多的慢小
		var speed = speeds[0];
		size = fontsizes[0];
		if(text.length > 6){
			speed = speeds[1];
			size = fontsizes[3];
		}
		if(text.length > 12){
			speed = speeds[2];
			size = fontsizes[2];
		}
		if(text.length > 24){
			speed = speeds[3];
			size = fontsizes[1];
		}
		span.style.fontSize = size+'px';
		var spanwidth = text.length * size + 80;
		
		//寻找插入弹幕的空行
		var index = 0;
		for(var i = 0; i < stageRows.length; ++i){
			//空行可直接添加
			if(stageRows[i].length <= 0){
				index = i;
				break;
			//弹幕已经跑远了可再次添加
			}else if(stageRows[i].length > 0 && stageRows[i][stageRows[i].length-1].count > spanwidth){
				index = i;
				break;
			//全满了插入第一行
			}else{
				index = 0;
			}
		}
		
		//弹幕重叠加速
		if(stageRows[index].length > 0){
			speed+=1;
		}
		span.style.top = (index*32)+6+'px';
		span.style.right = -(spanwidth-100)+'px';
		
		stageRows[index].push({count:0,speed:speed,ele:span});
		
		//重新唤醒计时器
		if(this.intervalQueue === null){
			move();
			console.log('开启计时器');
		}
	}
	
	//添加弹幕
	this.add = function(msg,color,size){
		if(typeof(color) == 'undefined'){
			color = colors[randNumber(0,colors.length-1)];
		}
		if(typeof(size) == 'undefined'){
			size = fontsizes[randNumber(0,fontsizes.length-1)];
		}
		this.insertToStageRows(msg,color,size);
	}
	
	//关闭弹幕
	this.turn = true;
	this.off = function(){
		clearInterval(this.intervalQueue);
		box.innerHTML = '';
		this.intervalQueue = null;
		for(var i in stageRows){
			stageRows[i] = [];
		}
		this.turn = false;
		console.log('danmu off...');
		return 'off';
	}
	
	//开启弹幕
	this.on = function(){
		this.turn = true;
		console.log('danmu on...');
		return 'on';
	}
}