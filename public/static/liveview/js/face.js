
/*
var face=new Face('facecontainer',{    //表情容器  必设置
	faceinput:'input',   //输入框      必设置
	facebutton:'face',   //颜文字按钮   必设置
	});
*/

var Face=function(facecontainer,param){
	var num=0;
	//生成表情列表
	this.facelist=function(){
		var face=['( ^ω^)','(๑•̀ㅂ•́)و✧','( ^ω^)','(๑•.•๑) ',
				'(⊙ˍ⊙)','( ¯3¯ )','╮(╯_╰)╭',
				'(°ཀ°)','(╥﹏╥)',
				'(≖ᴗ≖๑)','(*.д)?','(ゝ∀･)b'
				,'(ﾒﾟДﾟ)ﾒ','(((ﾟдﾟ)))','(←_←)'];
		var facetitle=['高兴','加油','期待','害羞',
						'发呆','亲亲','无奈','吐血',
						'流泪','流汗','疑惑',
						'点赞','愤怒','惊讶','左边'];
		num=face.length;
		var spanlist=[];
		for(var i=0;i<face.length;i++){
			var Span=document.createElement('span');
			var facewrap=document.getElementById(facecontainer);
			spanlist.push(Span);
			facewrap.appendChild(spanlist[i]);
			var arrface=facewrap.getElementsByTagName('span');
			arrface[i].innerHTML=face[i];
			arrface[i].title=facetitle[i];
		}
	}
	this.facelist();
	
	//输入并发送表情
	this.sendface=function(){
		function getCursortPosition (ctrl) {       //获取光标位置函数
			var CaretPos = 0;	// IE Support
			if (document.selection) {
			ctrl.focus ();
				var Sel = document.selection.createRange ();
				Sel.moveStart ('character', -ctrl.value.length);
				CaretPos = Sel.text.length;
			}
			// Firefox support
			else if (ctrl.selectionStart || ctrl.selectionStart == '0')
				CaretPos = ctrl.selectionStart;
			return (CaretPos);
		}
		var arrface=document.getElementById(facecontainer).getElementsByTagName('span');
		var input=document.getElementById(param.faceinput);
		for(var i=0;i<arrface.length;i++){
			arrface[i].index=i;
			arrface[i].onclick=function(){
				pos = getCursortPosition(input);
				s = input.value;
				input.value = s.substring(0, pos)+arrface[this.index].innerHTML+s.substring(pos);
				document.getElementById('facecontainer').style.display='none';
			}
		}    
		input.onclick=function(e){
			if(e&&e.stopPropagation){
			 	e.stopPropagation();
			}
		}
	}
	this.sendface();
	
	//显示表情框
	var facelistoff=1;
	this.facelistshow=function(e){
		var showface=document.getElementById(param.facebutton);
		if(facelistoff){	
			document.getElementById(facecontainer).style.display='block';
			facelistoff=0;
		}else{
			document.getElementById(facecontainer).style.display='none';
			facelistoff=1;
		}
		if(e&&e.stopPropagation){
			 e.stopPropagation();
		}
	}
	this.facehide=function(){
		window.onclick=function(){
			if(facelistoff==0){
				document.getElementById(facecontainer).style.display='none';
				facelistoff=1;
			}
		}
		document.body.onclick=function(){
			if(facelistoff==0){
				document.getElementById(facecontainer).style.display='none';
				facelistoff=1;
			}
		}
		}
}