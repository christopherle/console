/**
 * Simplified Chinese translation for bootstrap-datetimepicker
 * Yuan Cheung <advanimal@gmail.com>
 */
;(function($){
	$.fn.datetimepicker.dates['zh-CN'] = {
			days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
			daysShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
			daysMin:  ["日", "一", "二", "三", "四", "五", "六", "日"],
			months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
			monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
			today: "今日",
			suffix: [],
			meridiem: ["上午", "下午"]
	};
	
if($('.datetime').length){
	$('.datetime').datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		language:'zh-CN',
		weekStart: 1,
		todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		minView:0,
		showMeridian: 1
	});
}

if($('.date_start').length){
	$('.date_start').datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		language:'zh-CN',
		weekStart: 1,
		todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		minView:0,
		showMeridian: 1
	});
}
if($('.date_end').length){
	$('.date_end').datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		language:'zh-CN',
		weekStart: 1,
		todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		minView:0,
		minDate: 1,
		showMeridian: 1
	}).on('changeDate',function(ev){
		var start = $('.date_start:first').val();
		var end = $('.date_end:first').val();
		if(start == end){
			var end = end.split(' ');
			var time = end[1].split(':');
			time[0] = parseInt(time[0])+4;
			if(time[0] > 23){
				time[0] = 23;
				time[1] = 59;
			}
			var datetime = end[0]+' '+time.join(':');
			$('.date_end:first').val(datetime);
			$('.date_end:first').datetimepicker('update');
		}
		
	});
}
}(jQuery));

