<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];

    return new Slim\Views\PhpRenderer($settings['template_path']);
};

$container['web'] = function ($c) {
    $settings = $c->get('settings')['renderer'];

    return new Slim\Views\PhpRenderer($settings['template_web']);
};

$container['kvstorage'] = function ($c) {
    $conn = new KVCache($c);

    return $conn->store;
};

$container['live'] = function ($c) {
    $settings = $c->get('settings')['renderer'];

    return new Slim\Views\PhpRenderer($settings['liveview_path']);
};

// view renderer
$container['rest'] = function ($c) {
    $settings = $c->get('settings');

    return new RestRequest($settings['restful_baseurl']);
};

// view renderer
$container['token'] = function ($c) {
    return new UserToken();
};

function getMaxCreate($level){
	if($level == 1){
		return 5;
	}else if($level == 2){
		return 15;
	}else if($level == 3){
		return 30;
	}else{
		return 5;
	}
}

function isMobile(){   
	$useragent=isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';    
	$useragent_commentsblock=preg_match('|\(.*?\)|',$useragent,$matches)>0?$matches[0]:'';      
	function CheckSubstrs($substrs,$text){    
		foreach($substrs as $substr)    
			if(false!==strpos($text,$substr)){    
				return true;    
			}    
			return false;    
	}  
	$mobile_os_list=array('Google Wireless Transcoder','Windows CE','WindowsCE','Symbian','Android','armv6l','armv5','Mobile','CentOS','mowser','AvantGo','Opera Mobi','J2ME/MIDP','Smartphone','Go.Web','Palm','iPAQ');  
	$mobile_token_list=array('Profile/MIDP','Configuration/CLDC-','160×160','176×220','240×240','240×320','320×240','UP.Browser','UP.Link','SymbianOS','PalmOS','PocketPC','SonyEricsson','Nokia','BlackBerry','Vodafone','BenQ','Novarra-Vision','Iris','NetFront','HTC_','Xda_','SAMSUNG-SGH','Wapaka','DoCoMo','iPhone','iPod');    
				
	$found_mobile = CheckSubstrs($mobile_os_list,$useragent_commentsblock) ||    
			  CheckSubstrs($mobile_token_list,$useragent);    
				
	if ($found_mobile){
		return true;
	}else{
		return false;
	}
}

//格式化时间
function formatSecond(int $time)
{
	$format = '';
	if ($time >= 86400) {
		$format .= floor($time / 86400).' 天 ';
		$time = ($time % 86400);
	}
	if ($time >= 3600) {
		$format .= floor($time / 3600).' 小时 ';
		$time = ($time % 3600);
	}
	if ($time >= 60) {
		$format .= floor($time / 60).' 分钟 ';
		$time = ($time % 60);
	}
	if ($time > 0) {
		$format .= floor($time).' 秒';
	}

	return trim($format);
}

function formatBeforAfter(int $time){
	$after = time() - $time();
	$name = $after > 0? '前' : '后';
	$format = formatSecondTime(abs($after));
	return $format.$name;
}
function getCurl($url){
	$ch=curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_HEADER,0);
    curl_setopt($ch,CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36');
    curl_setopt($ch,CURLOPT_ENCODING,'GZIP');
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
    $output=curl_exec($ch);
    curl_close($ch);
    $rst = json_decode($output);
    return $rst;
}

function postCurl($url,$data){
    $headers = array('Content-type: application/json');
    $ch=curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36');
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch,CURLOPT_AUTOREFERER,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    $tmpInfo=curl_exec($ch);
    if(curl_errno($ch)){
        return curl_error($ch);
    }
    curl_close($ch);
    return $tmpInfo; 
}

function start_session($id,$expire = 0) 
{ 
    if ($expire == 0) { 
        $expire = ini_get('session.gc_maxlifetime'); 
    } else { 
        ini_set('session.gc_maxlifetime', $expire); 
    } 
    if (empty($_COOKIE[$id])) { 
        session_set_cookie_params($expire); 
        session_start(); 
    } else { 
        session_start(); 
        setcookie($id, session_id(), time() + $expire); 
    } 
} 
function http(){  
    if(!isset($_SERVER['HTTPS']))  return 'http:';  
    if($_SERVER['HTTPS'] === 1){  //Apache  
        return 'https:';  
    }elseif($_SERVER['HTTPS'] === 'on'){ //IIS  
        return 'https:';  
    }elseif($_SERVER['SERVER_PORT'] == 443){ //其他  
        return 'https:';  
    }  
    return 'http:';  
}  