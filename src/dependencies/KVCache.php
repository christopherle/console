<?php
/**
 * Slim Framework (http://slimframework.com).
 *
 * @link      https://moredoo.com
 *
 * @copyright Copyright (c) 2011-2015 Moredoo
 * @license   https://github.com/slimphp/PHP-View/blob/master/LICENSE.md (MIT License)
 */

/**
 * Php Json View.
 *
 * Render Restful api
 */
class KVCache
{
    protected $container;
    public $store;

    public function __construct($container)
    {
        $this->container = $container;
        $dsn = [
            'path' => $container->get('redis'),
            'expire' => 86400, //缓存有效期
        ];
        $this->store = new RedisCache($dsn);
        // $dsn = [
        //     'path' => dirname(dirname(dirname(__FILE__))).'/storages/caches/', //缓存位置
        //     'expire' => 86400, //缓存有效期
        // ];
        // $this->store = new FileCache($dsn);
    }
}

use Predis\Client;
class RedisCache
{
    private $_redis;
    private $_expire;
    private $_prex;

    public function __construct($dsn) {
        $pwd = $dsn['path']['password'];
        unset($dsn['path']['password']);
        $this->_redis = new Client($dsn['path']);
        if (isset($dsn['expire'])) {
            $this->_expire = intval($dsn['expire']);
        }
        $this->_redis->auth($pwd);
    }

    public function setDomain($domain)
    {
        $this->_prex = $domain.'_';
    }

    public function set($name, $value, $expire = 0) {
        if($expire <= 0) {
            $expire = $this->_expire;
        }
        if(is_array($value)) {
            $value = json_encode($value);
        }
        $this->_redis->setex($this->_prex.$name, $expire, $value);
    }

    public function get($name) {
        $value = $this->_redis->get($this->_prex.$name);
        if(! is_null(json_decode($value))) {
            $value = json_decode($value, true);
        }
        return $value;
    }

    public function del($name) {
        $this->_redis->del($name);
    }
    public function getExpire($name){
        return $this->_redis->ttl($name);
    }
}

//文件缓存 - 临时使用
class FileCache
{
    private $_path;
    private $_expire = 0;
    private $_suffix = '.cache.php';
    private $_file;

    public function __construct($dsn)
    {
        $this->_path = $dsn['path'];
        if (!is_dir($this->_path)) {
            if (!mkdir($this->_path)) {
                trigger_error('缓存目录创建失败', E_USER_ERROR);
            }
        }
        if (isset($dsn['expire'])) {
            $this->_expire = intval($dsn['expire']);
        }
    }

    public function setDomain($domain)
    {
        $this->_file = $this->_path.$domain.'.';
    }

    public function set($name, $value, $expire = 0)
    {
        if ($expire > 0) {
            $expire += time();
        } elseif ($this->_expire > 0) {
            $expire = $this->_expire + time();
        }
        $data = array('time' => $expire, 'data' => $value);
        $string = "<?php \r\n return ".var_export($data, true).';';
        $filename = $this->_file.$name.$this->_suffix;
        $fp = fopen($filename, 'wb');
        if ($fp) {
            flock($fp, LOCK_EX);
            fwrite($fp, $string);
            flock($fp, LOCK_UN);
            fclose($fp);

            return true;
        } else {
            return false;
        }
    }

    public function get($name)
    {
        $filename = $this->_file.$name.$this->_suffix;
        if (!is_file($filename)) {
            return false;
        }
        $data = include $filename;
        if ($data['time'] > 0 && $data['time'] < time()) {
            unlink($filename);

            return false;
        }

        return $data['data'];
    }

    public function del($name)
    {
        $filename = $this->_file.$name.$this->_suffix;
        if (is_file($filename)) {
            unlink($filename);
        }
    }

    public function flush()
    {
        $dh = opendir($this->_path);
        while ($file = readdir($dh)) {
            if ($file != '.' && $file != '..' && !is_dir($file)) {
                unlink($this->_path.$file);
            }
        }
    }
}
