<?php
/**
 * REST client.
 */
class RestRequest
{
    private $ch;
    public $baseUrl;
    public $responseInfo;
    public function __construct($baseUrl)
    {
        // set the base URL for the API endpoint
        $this->baseUrl = $baseUrl;
        // initialize the cURL resource
        $this->ch = curl_init();
        // set cURL and credential options
        curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl);
        // set headers
        $headers = array('Content-type: application/json');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        // return transfer as string
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
    }
    public function __destruct()
    {
        curl_close($this->ch);
    }
    public function get($url)
    {
        return $this->executeRequest($url, 'GET');
    }
    public function post($url, $data)
    {
        return $this->executeRequest($url, 'POST', $data);
    }
    public function put($url, $data)
    {
        return $this->executeRequest($url, 'PUT', $data);
    }
    public function delete($url)
    {
        return $this->executeRequest($url, 'DELETE');
    }

    public function executeRequest($url, $method, $data = null)
    {
        // set the full URL for the request
        curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl.'/'.$url);
        switch ($method) {
            case 'GET':
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                break;
            case 'POST':
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'PUT':
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'DELETE':
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default:
                break;
        }
        // execute the request
        $response = curl_exec($this->ch);
        // store the response info including the HTTP status
        // 400 and 500 status codes indicate an error
        $this->responseInfo = curl_getinfo($this->ch);
        //$httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

        // todo : add support in constructor for contentType {xml, json}
        $data = json_decode($response);
		if( ! is_array($data) && empty($data)){
			throw new Exception($response,101);
		}else{
			return $data;
		}
    }
}
