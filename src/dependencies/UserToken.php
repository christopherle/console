<?php

class UserToken
{
    public function __construct()
    {
        @session_start();
    }
	
	public function getSpectator(){
		if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			$ip = (array)explode(',',$ip);
			$ip = $ip[0];
		}else if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}else{
			$ip = '127.0.0.1';
		}
		$agent = isset($_SERVER['HTTP_USER_AGENT'])? $_SERVER['HTTP_USER_AGENT'] : mt_rand(1000,99999);
		if($ip == 'undefined'){
			$uid = md5($ip.$agent.time());
			$spec = ['uid'=>$uid,'ip'=>$ip,'province'=>'未知','city'=>'未知'];
			$_SESSION[$ip] = $spec;
		}else if(!isset($_SESSION[$ip])){
			$uid = md5($ip.$agent.time());
			$http = new RestRequest('http://int.dpool.sina.com.cn/');
			$ipinfo = $http->get('/iplookup/iplookup.php?format=json&ip='.$ip);
			if(!is_object($ipinfo)) $ipinfo = (object)array();
			if(empty($ipinfo->country)) $ipinfo->country = '未知';
			if(empty($ipinfo->province)) $ipinfo->province = '未知';
			if(empty($ipinfo->city)) $ipinfo->city = '未知';
			$spec = ['uid'=>$uid,'ip'=>$ip,'province'=>$ipinfo->province,'city'=>$ipinfo->city];
			$_SESSION[$ip] = $spec;
		}
		return $_SESSION[$ip];
	}
	
	public function getChatUserinfo(){
		$ipinfo = $this->getSpectator();
		if($ipinfo['city'] == '未知') $ipinfo['city'] = '火星';
		return ['uid'=>$ipinfo['uid'],'nickname'=>$ipinfo['city'].'网友'];
	}
	//判断是否登录
    public function checkSignStatus()
    {
        if (empty($_SESSION['sigininfo'])) {
            header('location:/login');
            exit;
        }
    }
	//得到用户登录信息
    public function getSignInfo()
    {
		if(isset($_SESSION['sigininfo'])){
			return $_SESSION['sigininfo'];
		}
    }

    public function update($userinfo)
    {
        $_SESSION['sigininfo'] = $userinfo;
    }

	// 获取用户订单信息
	public function getRechargeInfo()
    {
        return $_SESSION['rechargeinfo'];
    }
}
