<?php
header('Access-Control-Allow-Origin:*');
header('Access-Control-Max-Age:1728000');
header('Access-Control-Allow-Methods:POST,PUT,GET,DELETE');
header('Access-Control-Allow-Headers:X-Requested-With,Content-Type,Accept');
date_default_timezone_set('Asia/Shanghai');

// web Routes
$app->get('/', function ($request, $response, $args) {
	$assign = [];
	$post = $request->getParams();
	if(empty($post['action'])){
		$post['action'] = 'home';;
		$tpl = 'index.html';
	}else{
		$tpl = $post['action'].'.html';
	}
	$settings = $this->get('settings');
    $assign['restful'] = $settings['restful_baseurl'];
	$assign['userinfo'] = $this->token->getSignInfo();
	$assign['params'] = $post;
    $this->web->render($response, $tpl, $assign);
});

$app->post('/articals/preview',function ($request, $response, $args) {
	$assign['cates'] = $this->rest->get("/v1/articals/cates/list");
	$artical = json_decode(json_encode($_POST));
	$artical->created_at = strtotime($artical->created_at);
	$assign['artical'] = $artical;
	$assign['userinfo'] = $this->token->getSignInfo();
	$assign['params'] = ['action'=>'wiki'];
	$this->web->render($response, 'preview.html',$assign);
});

$app->post('/consult', function ($request, $response, $args) {
	$params = $request->getParams();
	$rst = $this->rest->post("/v1/consults",$params);
	echo json_encode($rst);
});

// Console Routes
$app->get('/console', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $userinfo = $this->token->getSignInfo();
	$msg = $this->rest->get('/v1/users/system/msg?token='.$userinfo['token']);
	$userinfo['msg'] = $msg;
	$balance = $this->rest->get("v1/money/balance/{$userinfo['userinfo']->id}?token={$userinfo['token']}");
	$userinfo['balance'] = isset($balance->data)? $balance->data : '0.00';
    $this->renderer->render($response, 'index.html', $userinfo);
});

$app->get('/welcome', function ($request, $response, $args) {
	$this->token->checkSignStatus();
	$signinfo = $this->token->getSignInfo();
	$userinfo = (array)$signinfo['userinfo'];//原$signinfo['userinfo']为对象，转换为数组
	$userinfo['mobile'] = substr_replace($userinfo['mobile'],'****',3,4);//替换字符串，隐藏完整号码
	//$email = explode('@',$userinfo['email']);
	//$userinfo['email'] = substr_replace($email[0],str_repeat('*',strlen($email[0])-2),2).'@'.$email[1];
	$assign = ['userinfo'=>(object)$userinfo];

	$balance = $this->rest->get("v1/money/balance/{$userinfo['id']}?token={$signinfo['token']}");//查询余额
	$assign['balance'] = isset($balance->data)? $balance->data : '0.00';
	
	$level = [1=>'普通用户',2=>'专业用户',3=>'企业用户'];
	$assign['level'] = $level[$userinfo['level']];//客户类型
	
	//安全度
	$safe = 100;

	$realname = $this->rest->get("v1/users/realname/{$userinfo['id']}?token={$signinfo['token']}");//未实名认证
    if(isset($realname->data) && $realname->data=='fail'){
        $safe -= 20;
        $assign['realname']=false;
    }else{
    	$assign['realname']=true;
    }

	if($userinfo['email_active'] != 1) $safe -= 20;//未填写email
	if($userinfo['mobile_active'] != 1) $safe -= 20;//未填写手机号
	$info = $this->rest->get("v1/users/userinfo/{$userinfo['id']}");//检查用户信息完整度
	foreach($info as $val){
		if(empty($val)){
			$safe -= 20;
			break;
		}
	}
	if($safe < 50){
		$assign['safe'] = ['safe_lv' =>1,'name'=>'低','descr'=>'存在风险，请完善资料'];
	}else if($safe >= 50 && $safe <= 80){
		$assign['safe'] = ['safe_lv' =>2,'name'=>'中','descr'=>'再接再励'];
	}else{
		$assign['safe'] = ['safe_lv' =>3,'name'=>'高','descr'=>'非常安全'];
	}
	
	//实名认证
	// $realname = $this->rest->get("v1/users/realname/{$signinfo['userinfo']->id}?token={$signinfo['token']}");
	// $assign['realname'] =isset($realname->data) && $realname->data == 'fail'? false : true;

	$dvrs = $this->rest->get("v1/service/dvrs?token=".$signinfo['token']);
	$assign['dvrs'] = $dvrs;
	//获取接口地址
	$settings = $this->get('settings');
	$restful = $settings['restful_baseurl'];
	//新闻1：新手入门 2：新闻动态
	$news = json_decode(file_get_contents($restful.'/v1/articals/list/1?start=0'),true);
	$assign['news'] = array_slice($news['list'],0,5);

	$news2 = json_decode(file_get_contents($restful.'/v1/articals/list/4?start=0'),true);
	$assign['news2'] = array_slice($news2['list'],0,5);
	
	// 用户统计信息
	$starttime = strtotime(date('Y-m-d 00:00:00'));
	$endtime = strtotime(date('Y-m-d 23:59:59'));
	$assign['pano_live'] = $this->rest->get("v1/streams/user/count/{$userinfo['id']}?starttime={$starttime}&endtime={$endtime}&type=pano_live");
	$assign['pano_vod'] = $this->rest->get("v1/streams/user/count/{$userinfo['id']}?starttime={$starttime}&endtime={$endtime}&type=pano_vod");

    $list = $this->rest->get("v1/streams?token={$signinfo['token']}");
    $assign['list'] = $list;
	

	$this->renderer->render($response, 'welcome.html', $assign);
});

$app->get('/qrcode', function ($request, $response, $args) {
	$size = $request->getParam('size')>0? $request->getParam('size') : 3;
	\PHPQRCode\QRcode::png(urldecode($_GET['url']),false,\PHPQRCode\Constants::QR_ECLEVEL_L,$size);
	exit;
});
$app->get('/captcha', function ($request, $response, $args) {
	$builder = new Gregwar\Captcha\CaptchaBuilder((string)mt_rand(1000,9999));
	// print_r($builder);
	// exit;
	$builder->setIgnoreAllEffects(false);
	$builder->setBackgroundColor(212,222,242);
	$builder->setMaxBehindLines(0);
	$builder->setMaxFrontLines(0);
	// $builder->setFontSize(15);
	$builder->build(80,46);
	header('Content-type: image/jpeg');
	$_SESSION['captcha'] = $builder->getPhrase();
	$builder->output();
	exit;
});
require 'routes/user.php';
require 'routes/stream.php';
require 'routes/money.php';
require 'routes/sso.php';
require 'routes/watch.php';
require 'routes/ticket.php';
require 'routes/wechat.php';
require 'routes/push.php';
// require 'routes/detu.php';
require 'routes/oss.php';