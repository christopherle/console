<?php
$app->get('/detu', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $signinfo = $this->token->getSignInfo();
    $userinfo = $signinfo['userinfo'];
    $assign['userinfo'] = $userinfo;
    //获取用户余额
    $balance = $this->rest->get("v1/money/balance/{$userinfo->id}?token={$signinfo['token']}");
    $assign['balance'] = isset($balance->data)? $balance->data : '0.00';
    //获取系统消息
    $assign['msg'] = $this->rest->get('/v1/users/system/msg?token='.$signinfo['token']);
    $assign['signinfo'] = json_encode($signinfo);
    $this->renderer->render($response, 'detu/center.html', $assign);
});

$app->map(['GET', 'POST'],'/detu/sign', function ($request, $response, $args) {
    if($_POST){
        // var_dump($_POST);
        // exit;
        $rst = $this->rest->post('v1/users/detu/register', $_POST);
        if(isset($rst->errno)){
            $assign = ['error'=>$rst->error,'mobile'=>$_POST['mobile'], 'password'=>$_POST['password']];
            $this->renderer->render($response, 'detu/register.html', $assign);
        }else{
            return $response->withStatus(301)->withHeader('Location', '/detu/login');
        }
    }else{
        $params = $request->getParams();
        $secretkey = '4c2cfc351f75b42229463b81d005551b';
        // $params['ts'] = '1489474943';
        // $sign = md5(sha1($secretkey.'detu'.$params['ts']));
        // echo $sign;
        if(isset($params['access_key']) && $params['access_key'] == md5(sha1($secretkey.'detu'.$params['ts']))){
            $assign = [];
            $this->renderer->render($response, 'detu/register.html', $assign);
        }else{
            echo 'deny';
        }
    }
});

$app->map(['GET', 'POST'], '/detu/login', function ($request, $response, $args) {
    if (!empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu');
    }
    if($_POST){
        if(empty($_POST['imgcode']) || strtolower($_POST['imgcode']) != strtolower($_SESSION['captcha'])){
            $assign = ['error'=>'图片验证码错误','username'=>$_POST['username'], 'password'=>$_POST['password']];
            $this->renderer->render($response, 'detu/login.html', $assign);
        }else{
            unset($_POST['imgcode']);
            $data = $this->rest->put('v1/users', $_POST);
            if(isset($data->errno)){
                $assign = ['error'=>$data->error,'username'=>$_POST['username'], 'password'=>$_POST['password']];
                $this->renderer->render($response, 'detu/login.html', $assign);
            }else{
                $this->token->update((array) $data);
                return $response->withStatus(301)->withHeader('Location', '/detu');
            }
        }
    }else{
        $assign = [];
        $this->renderer->render($response, 'detu/login.html', $assign);
    }
});


// 发送短信验证码
$app->post('/detu/regcode', function($request, $response, $args) {
    $post = $request->getParams();
    if(empty($post['imgcode']) || strtolower($post['imgcode']) != strtolower($_SESSION['captcha'])){
        return json_encode(['errno'=>101,'error'=>'图片验证码错误']);
    }
    $query = ['username'=>$post['username']];
    return json_encode($this->rest->post('v1/users/regcode', $query));
});


$app->map(['GET', 'POST'], '/detu/stream/create', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $userinfo = $this->token->getSignInfo();
    $id = $userinfo['userinfo']->id;
    $post = $request->getParams();
    $assign['id'] = $id;
    if (empty($post)) {
        $assign['post'] = ['alias' => '', '', 'start_live' => time(), 'stop_live' => time()+14400];
    } else {
        $post['start_live'] = strtotime($post['start_live']);
        $post['stop_live'] = strtotime($post['stop_live']);
        $assign['post'] = $post;
        $userinfo = $this->token->getSignInfo();
        $post['token'] = $userinfo['token'];
        $post['name'] = $id.substr(md5(time().uniqid()), rand(0, 16), 16 - strlen($id));
        $post['cover'] = "http://{$_SERVER['HTTP_HOST']}/static/console/img/cover.jpg";
        $post['h5_cover'] = "http://{$_SERVER['HTTP_HOST']}/static/console/img/h5_cover.jpg";
        $data = $this->rest->post('v1/streams', $post);
        if (!isset($data->errno)) {
            $item = $this->rest->get("v1/streams/{$data->data}");
            $assign['item'] = (array)$item;
            $assign['userinfo'] = $userinfo['userinfo'];
            $this->renderer->render($response, 'detu/create-success.html', $assign);
            return;
        } else {
            // $assign['error'] = $data->error;
            // $this->renderer->render($response, 'error.html', ['back' => '/stream/my', 'msg' => $data->error]);
            
            // return json_encode(['rst'=>'0','tip'=>$data->error]);
            $assign['post'] = $post;
            $assign['error'] = $data->error;
        }
    }
    
    $this->renderer->render($response, 'detu/create.html', $assign);
});

$app->get('/detu/stream/list', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $userinfo = $this->token->getSignInfo();
    $list = $this->rest->get("v1/streams?token={$userinfo['token']}");
    $assign = ['list' => $list];
    $assign['userinfo'] = $userinfo['userinfo'];
    // var_dump($list);
    $this->renderer->render($response, 'detu/stream-list.html', $assign);
});

$app->map(['GET', 'POST'] ,'/detu/realname', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $assign = [];
    $signinfo = $this->token->getSignInfo();
    $post = $request->getParams();
    $assign['post'] = $this->rest->get("v1/users/realname/{$signinfo['userinfo']->id}?token={$signinfo['token']}");
    if(!empty($post)){
        $query = ['token'=>$signinfo['token'],'user_id'=>$signinfo['userinfo']->id,'type'=>$post['utype'],'idcode'=>$post['idNumber'],'ida'=>$post['id1'],'idb'=>$post['id2'],'company'=>$post['company'],'company_id'=>$post['businessLicence'],'company_code'=>$post['organization'],'license'=>$post['businessFile'],'status'=>0];
        $data = $this->rest->post("v1/users/realname/{$signinfo['userinfo']->id}", $query);
        if(!isset($data->errno)){
            return $response->withStatus(301)->withHeader('Location', '/detu/realname');
        }
    }
    $settings = $this->get('settings');
    $assign['uploadurl'] = $settings['storage_baseurl'].'/addimage.php?id='.$signinfo['userinfo']->id.'&token='.$settings['storage_token'];
    $this->renderer->render($response, 'detu/realname.html',$assign);
});

$app->get('/detu/realname/cancel', function($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $signinfo = $this->token->getSignInfo();
    $post = $request->getParams();
    $rst = $this->rest->delete("v1/users/realname/{$post['id']}?token={$signinfo['token']}",$query);
    if(isset($rst->errno)){
        exit('重置失败');
    }else{
        return $response->withStatus(301)->withHeader('Location', '/detu/realname');
    }
});

$app->get('/detu/operation', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $this->renderer->render($response, 'detu/operation.html');
});

$app->get('/detu/operation360', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $this->renderer->render($response, 'detu/operation-360.html');
});

$app->get('/detu/delete/{id}', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $userinfo = $this->token->getSignInfo();
    $list = $this->rest->delete("v1/streams/{$args['id']}?token={$userinfo['token']}");
    if (isset($list->errno)) {
        throw new Exception($list->error, 101);
    } else {
        return $response->withStatus(301)->withHeader('Location', '/detu/stream/list');
    }
});

$app->get('/detu/recharge', function ($request, $response, $args) {
    $params = $request->getParams();
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/detu/login');
    }
    $signinfo = $this->token->getSignInfo();
    $assign['openid'] = $signinfo['userinfo']->openid;
    $this->renderer->render($response, 'detu/recharge.html',$assign);
});

$app->get('/detu/pay', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $user = $this->token->getSignInfo();
    $v_mid = '110180220003';
    $params = $request->getParams();
    $orderid =  date('Ymd').'-'.$v_mid.'-'.$user['userinfo']->id.date('His');
    $postdata = ['token'=>$user['token'],'orderid'=>$orderid,'money'=>$params['money'],'type'=>'unionpay'];
    $rst = $this->rest->post("v1/money/orderid/{$user['userinfo']->id}", $postdata);
    if(isset($rst->errno)){
        exit($rst->error);
    }
    //发送支付请求，跳转至网银进行支付
    $assign['token'] = $user['token'];
    $assign['key'] = 'J4ezfWoEMRKXPKetyw5aHDmialnrD5ul';
    $assign['v_mid'] = $v_mid;
    $assign['v_oid'] =$orderid;
    $assign['v_amount'] = $params['money'];
    $assign['v_moneytype'] = 'CNY';
    $assign['v_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/detu/pay/receive';
    $assign['remark2'] = '[url:=http://'.$_SERVER['HTTP_HOST'].'/detu/pay/autoreceive]';
    $assign['v_md5info'] = strtoupper(md5($assign['v_amount'].$assign['v_moneytype'].$assign['v_oid'].$assign['v_mid'].$assign['v_url'].$assign['key']));
    $this->renderer->render($response, 'money/unionpay.html', $assign);
});

$app->any('/detu/pay/receive', function ($request, $response, $args) {
    $key='J4ezfWoEMRKXPKetyw5aHDmialnrD5ul';
    $v_oid       = trim($_REQUEST['v_oid']);       // 商户发送的v_oid定单编号   
    $v_pmode     = trim($_REQUEST['v_pmode']);    // 支付方式（字符串）   
    $v_pstatus   = trim($_REQUEST['v_pstatus']);   //  支付状态 ：20（支付成功）；30（支付失败）
    $v_pstring   = trim($_REQUEST['v_pstring']);   // 支付结果信息 ： 支付完成（当v_pstatus=20时）；失败原因（当v_pstatus=30时,字符串）； 
    $v_amount    = trim($_REQUEST['v_amount']);     // 订单实际支付金额
    $v_moneytype = trim($_REQUEST['v_moneytype']); //订单实际支付币种    
    $token     = trim($_REQUEST['remark1' ]);      //备注字段1
    $remark2     = trim($_REQUEST['remark2' ]);     //备注字段2
    $v_md5str    = trim($_REQUEST['v_md5str' ]);   //拼凑后的MD5校验值  
    $md5string=strtoupper(md5($v_oid.$v_pstatus.$v_amount.$v_moneytype.$key));

    /**
     * 判断返回信息，如果支付成功，并且支付结果可信，则做进一步的处理
     */
    if ($v_md5str==$md5string){
        if($v_pstatus=="20"){
            //支付成功，可进行逻辑处理！
            //商户系统的逻辑处理（例如判断金额，判断支付状态，更新订单状态等等）......
            $putdata = ['token'=>$token,'orderid'=>$v_oid,'descr'=>"银联订单号：{$v_oid}"];
            $data = $this->rest->put('v1/money/recharge', $putdata);
            if(isset($data->errno)){
                if($data->errno == '201'){
                    $assign['result'] = '订单已经完成';
                }else{
                    $assign['result'] = $data->error;
                }
                $assign['style'] = "color:red";
                
            }else{
                $assign['result'] = '您已成功支付';
                $assign['style'] = "color:rgb(106, 199, 29)";
            }
        }else{
            $assign['result'] = "支付失败";
            $assign['style'] = "color:red";
        }
    }else{
        $assign['result'] = "校验失败,数据可疑";
        $assign['style'] = "color:red";
    }
    $this->renderer->render($response, 'detu/pay-success.html', $assign);
});



$app->any('/detu/pay/autoreceive', function ($request, $response, $args) {
    $key='J4ezfWoEMRKXPKetyw5aHDmialnrD5ul';
    $v_oid       = trim($_REQUEST['v_oid']);
    $v_pmode     = trim($_REQUEST['v_pmode']);
    $v_pstatus   = trim($_REQUEST['v_pstatus']);
    $v_pstring   = trim($_REQUEST['v_pstring']);
    $v_amount    = trim($_REQUEST['v_amount']);
    $v_moneytype = trim($_REQUEST['v_moneytype']);
    $token     = trim($_REQUEST['remark1']);
    $remark2     = trim($_REQUEST['remark2']);
    $v_md5str    = trim($_REQUEST['v_md5str']);
    $md5string=strtoupper(md5($v_oid.$v_pstatus.$v_amount.$v_moneytype.$key)); //拼凑加密串
    if ($v_md5str!=$md5string || $v_pstatus!="20"){
           exit("error");
    }
    $putdata = ['token'=>$token,'orderid'=>$v_oid,'descr'=>"银联订单号：{$v_oid}"];
    $data = $this->rest->put('v1/money/recharge', $putdata);
    if(isset($data->errno)){
        echo "error";
    }else{
        echo "ok";
    }
});

$app->get('/detu/system/msg', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $signinfo = $this->token->getSignInfo();
    $params = $request->getParams();
    $show = isset($params['page']) ? $params['page'] : 1;
    $list = $this->rest->get("v1/users/system/msg/update?token={$signinfo['token']}&page=".$show);
    $assign['list'] = $list;
    $this->renderer->render($response, 'detu/msg.html', $assign);
});

$app->post('/detu/exchange', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $signinfo = $this->token->getSignInfo();
    if($signinfo['userinfo']->id != $_POST['user_id']){
        return json_encode(['error'=>'没有兑换权限']);
    }
    $user_id = $signinfo['userinfo']->id;
    $secretkey = '4c2cfc351f75b42229463b81d005551b';
    $time = time();
    $access_key = md5(sha1($secretkey.'detu'.$time));
    $data = ['code'=>$_POST['excode'], 'access_key' => $access_key, 'ts'=>$time];
    $url = 'http://rest.api.yyport.com/v1/logs/add';
    // $detu_rst = postCurl($url, $data);
    $detu_rst = json_encode(['rstcode'=>'1', 'money'=>'1', 'msg'=>'兑换成功']);
    $detu_rst = json_decode($detu_rst);
    if($detu_rst->rstcode == '0'){
        return json_encode(['error'=>$detu_rst->msg]);
    }
    $params = ['user_id'=>$user_id, 'money'=>$detu_rst->money, 'token'=>$signinfo['token'], 'orderid'=>$user_id.time().rand(1000,9999),'code'=>$_POST['excode']];
    $rst = $this->rest->put("v1/money/detu?token={$signinfo['token']}", $params);
    return json_encode($rst);
});