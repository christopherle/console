<?php
header("Content-type: text/html; charset=utf-8");
/**
 *功能：配置文件
 *版本：1.0
 *修改日期：2014-06-26
 '说明：
 '以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己的需要，按照技术文档编写,并非一定要使用该代码。
 '该代码仅供学习和研究爱贝云计费接口使用，只是提供一个参考。
 */
 
//爱贝商户后台接入url
// $coolyunCpUrl="http://pay.coolyun.com:6988";
$iapppayCpUrl="http://ipay.iapppay.com:9999";
//登录令牌认证接口 url
$tokenCheckUrl=$iapppayCpUrl . "/openid/openidcheck";

//下单接口 url
// $orderUrl=$coolyunCpUrl . "/payapi/order";
$orderUrl=$iapppayCpUrl . "/payapi/order";

//支付结果查询接口 url
$queryResultUrl=$iapppayCpUrl ."/payapi/queryresult";

//契约查询接口url
$querysubsUrl=$iapppayCpUrl."/payapi/subsquery";

//契约鉴权接口Url
$ContractAuthenticationUrl=$iapppayCpUrl."/payapi/subsauth";

//取消契约接口Url
$subcancel=$iapppayCpUrl."/payapi/subcancel";
//H5和PC跳转版支付接口Url
$h5url="https://web.iapppay.com/h5/exbegpay?";
$pcurl="https://web.iapppay.com/pc/exbegpay?";

//应用编号
$appid="300555667";
//应用私钥
$cpvkey="MIICXgIBAAKBgQCcPmE4LmhDnD42JlUwH2YjaqvBzxY29wz84Dgiocz5odXx4pHP6FsgVLCgs1BduI1TuC30n8M1FkgEH3eXShPVHuAQq6bkHZwBuaRnfgEcjc0kqLq6gpml4j6mzZ0JjfDXbfYHymJ/uaMFr+HLS5lw5SpRCxUQf6uyEpuJ9PqmqQIDAQABAoGBAJoInYvnpH7TMDnuPJm+utE7vqzJXls94B4FZxItdYi9VdCrz5iqt/v9wkwLu7VJ1nqs3xGF18skdkcqRppuO3GVzlxt2sulrVrcw1lYzOgRJp4vPBAoyAYkLbgAHbaHdbdl3ZkpeDHdskl2jhdQ3IqGqtiU+qu+i3FNzp10Yi0JAkEA72FHSYZOYx666vfSLzBNcO1OKY+/xsI53Lxzn+HB0Fw7JUERLrprn9wpYOzWAOCylTeiFFKs+VhkVVk9X6fVgwJBAKcXcVVrZNMfYJ3p/1g4k5eEx8od1cthclw8CztnBEx1Lbs8POTTY2zRFBv07U7j90TC2K3TUgvjPIW7h9aph2MCQQCz0xAKW6pnK56fBKwQZujF/H7LGgGm/iEZgTga0y0VqTOFOOv/fmt7G5i6BzB8WE/PaIuJWNI4Pf6hXviL8zihAkBIxR7osrLgd6PeYlCAnXM54+wXDFNWFgR2QvgpUL5Ahi/6gUuL5LxCYfQ+ELdhSksgOTOg0I3E+5waKggI5yA9AkEA1R2mHpdonwtdz3E4iB3lWH4hRIKwaFdk+SkgNpqBqG6g4+KHmTNpe2runzBZ69sSBYUgGyCgMkm2nZpZ9rFI9w==";

//平台公钥
$platpkey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCndtdB+jNPlyzye78YJoJdLdNYL0DKQKZT0Qc7tNThDG8bZRxFjhABq/uzUnwKsVcAChCvCBsxiWrQCJU/K4CThYT67XVWUn0qSAeW/g6DStTPVJOjGpbCdv2hFWYH2f6CK3ZWbUDg26YlOojcJxrIq4oXzXwj594Ijmq+kdgE2wIDAQAB";
?>
