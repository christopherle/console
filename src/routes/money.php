<?php
// 消费记录
$app->map(['GET', 'POST'], '/money/details', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $user = $this->token->getSignInfo();
	$params = $request->getParams();
	if(empty($params['tag'])) $params['tag'] = '';
	$params['token']= $user['token'];
	$params['starttime'] = empty($params['starttime'])? strtotime(date('Y-m-d 00:00:00')) : strtotime($params['starttime']);
	$params['endtime'] = empty($params['endtime'])? strtotime(date('Y-m-d 23:59:59')) : strtotime($params['endtime']);
	$current = empty($params['page'])? 1 : $params['page'];
	$params['start'] = ($current-1)*20;
	$query = http_build_query($params);
    $logs = $this->rest->get("v1/money/logs/{$user['userinfo']->id}?{$query}");
	$page = new Page($logs->total,$current);
	$tags = ['pano_live'=>'直播消费','pano_vod'=>'点播消费','pano_lps'=>'多平台推流'];
	$assign = ['logs'=>$logs,'tags'=>$tags,'page'=>$page,'params'=>$params];
	$this->renderer->render($response, 'money/details.html', $assign);
});

// 支付页面
$app->map(['GET', 'POST'], '/money/recharge', function ($request, $response, $args) {
    $post = $request->getParams();
	$signinfo = $this->token->getSignInfo();
	$this->renderer->render($response, 'money/recharge.html', []);
});

// 线下支付页面
$app->map(['GET', 'POST'], '/money/offline_recharge', function ($request, $response, $args) {
    $post = $request->getParams();
	$signinfo = $this->token->getSignInfo();
	if(!empty($post)){
		$list = $this->rest->post("v1/money/offline/recharge?token={$signinfo['token']}",$post);
	
		if (isset($list->errno)) {
			$this->renderer->render($response, 'error.html', ['back' => '/money/offline_recharge', 'msg' => '信息提交失败，请重试或联系客服']);
		}else{
			$this->renderer->render($response, 'success.html', ['back' => '/money/offline/recharge', 'msg' => '提交成功，请等待财务审核']);
		}
		return;
	}
	$settings = $this->get('settings');
	$assign = array();
	$assign['userinfo'] = $signinfo['userinfo'];
	$assign['uploadurl'] = $settings['storage_baseurl'].'/addimage.php?id='.$signinfo['userinfo']->id.'&token='.$settings['storage_token'];
	$this->renderer->render($response, 'money/offline_recharge.html', $assign);
});

// 支付成功回调页面
// doc https://www.iapppay.com/g-resultmsg.html
$app->map(['GET', 'POST'], '/money/nofity', function ($request, $response, $args){
	require 'iapplib/base.php';
	require 'iapplib/config.php';

	//接收参数
	$post = $request->getParams();
	if(empty($post)){
		return 'FAILURE';
	}

	//参数检查
	if (stripos("%22",$transdata)) {
		//判断接收到的数据是否做过 Urldecode处理，如果没有处理则对数据进行Urldecode处理
		$post = array_map ('urldecode',$post);
	}

	//把数据组装成验签函数要求的参数格式
	$respData = 'transdata='.$post['transdata'].'&sign='.$post['sign'].'&signtype='.$post['signtype'];
	//验证签名
	if (! parseResp($respData, $platpkey, $respJson)) {
		return 'FAILURE';
	}

	//充值订单
	$transdata = json_decode($post['transdata']);
	if($transdata->result != 0){
		return "FAILURE";
	}
	
	$putdata = ['token'=>$transdata->cpprivate,'orderid'=>$transdata->cporderid,'descr'=>"爱贝订单号：{$transdata->transid}，商户订单号：{$transdata->cporderid}"];
	$data = $this->rest->put('v1/money/recharge', $putdata);
	return 'SUCCESS';
});

// 订单生成
// 1. ajax获取数据，然后生成订单数据。
// 2. 向支付平台请求订单。
// 3. 构造支付链接并ajax返回。
// doc https://www.iapppay.com/g-book.html
$app->map(['GET', 'POST'], '/money/order', function ($request, $response, $args) {
	require 'iapplib/base.php';
	require 'iapplib/config.php';

	$post = $request->getParams();
	if(empty($post['recharge']) || !is_numeric($post['recharge']) || $post['recharge'] <= 0){
		return json_encode(['code'=>101,'msg'=>'金额不正确，请输入正确的金额。']);
	}

	// url头部信息
	$http = (isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']!='off') ? 'https://' : 'http://';
	$servername = $_SERVER["SERVER_NAME"];
	$port = $_SERVER["SERVER_PORT"] == 80 ? '' : ':' . $_SERVER["SERVER_PORT"];
	$baseurl = $http . $servername . $port;

	//生成订单
	$signinfo = $this->token->getSignInfo();
	$orderid = $signinfo['userinfo']->id . date('YmdHis') . mt_rand(100,999);
	$money = number_format($post['recharge'],2,'.','');
	$postdata = ['token'=>$signinfo['token'],'orderid'=>$orderid,'money'=>$money,'type'=>'iapp'];
	$this->rest->post("v1/money/orderid/{$signinfo['userinfo']->id}", $postdata);

	// 进行计费处理
	$header = $request->getHeaders();
	$userAgent = $header['HTTP_USER_AGENT'][0];

	// 1.设置获取transid的请求数据
	$orderData = [];
	$orderData['appid'] = $appid;
	$orderData['appuserid'] = $appid;
	$orderData['cporderid'] = $orderid;
	$orderData['cpprivateinfo'] = $signinfo['token'];//用户token
	$orderData['currency'] = 'RMB';
	$orderData['notifyurl'] = $baseurl.'/money/nofity';
	$orderData['price'] = floatval($money);
	$orderData['waresid'] = 1;
	$orderData['waresname'] = '目睹控制台充值订单号:'.$orderid;

	// 2.下单获取transid 交易流水号
	$orderReqData = composeReq($orderData, $cpvkey);
	$orderRespData = request_by_curl($orderUrl, $orderReqData, $userAgent);

	// 验签数据并且解析返回报文
	$transid = null;
	if (! parseResp($orderRespData, $platpkey, $respJson)) {
		return json_encode(['code'=>102,'msg'=>"订单生成失败！"]);
	} else if (! empty($respJson->code)) {
		return json_encode(['code'=>103,'msg'=>'支付错误！请重新下单，错误详情：'.$respJson->code.':'.$respJson->errmsg]);
	} else {
		// echo "success.服务端下单完成。";
		// 下单成功之后获取 transid
		$transid = $respJson->transid;
	}

	// 3.下单生成url
	$transdata = [];
	$transdata['transid'] = $transid;
	$transdata['redirecturl'] = $baseurl.'/money/details';	// 支付成功后跳转页面
	$transdata['cpurl'] = $baseurl;
	$content = json_encode($transdata);

	//格式化key，建议将格式化后的key保存，直接调用
	$vkey = formatPriKey($cpvkey);

	//生成签名
	$sign = sign($content, $vkey);

	//组装请求报文，目前签名方式只支持RSA这一种
	$reqData = "transdata=".urlencode($content)."&sign=".urlencode($sign)."&signtype=RSA";

	$payurl = $pcurl.$reqData;
	
	$response = ['code'=>0,'data'=>$payurl];

	return json_encode($response);
});

$app->get('/money/recharge/log', function ($request, $response, $args) {
	$this->token->checkSignStatus();
    $user = $this->token->getSignInfo();
	$params = $request->getParams();
	if(empty($params['type'])) $params['type'] = '1';
	// var_dump(expression);
	$params['token']= $user['token'];
	$params['starttime'] = empty($params['starttime'])? strtotime(date('Y-m-d 00:00:00')) : strtotime($params['starttime']);
	$params['endtime'] = empty($params['endtime'])? strtotime(date('Y-m-d 23:59:59')) : strtotime($params['endtime']);
	$current = empty($params['page'])? 1 : $params['page'];
	$params['start'] = ($current-1)*20;
	if($params['type'] == '1'){
		$params['tag'] = 'recharge';
		$query = http_build_query($params);
		$logs = $this->rest->get("v1/money/logs/{$user['userinfo']->id}?{$query}");
	}else{
		$params['status'] = '1';
		$query = http_build_query($params);
		$logs = $this->rest->get("v1/money/recharge/logs/{$user['userinfo']->id}?{$query}");
	}
	$page = new Page($logs->total,$current);
	$tags = ['pano_live'=>'直播消费','pano_vod'=>'点播消费','recharge'=>'充值','wxpay'=>'微信付款'];
	$assign = ['logs'=>$logs,'tags'=>$tags,'page'=>$page,'params'=>$params];
	// var_dump($logs);
	$this->renderer->render($response, 'money/record.html', $assign);
});
$app->get('/money/offline/recharge', function ($request, $response, $args) {
	$this->token->checkSignStatus();
    $user = $this->token->getSignInfo();
	$params = $request->getParams();
	if(empty($params['type'])) $params['type'] = '';
	$params['token']= $user['token'];
	// $params['starttime'] = empty($params['starttime'])? strtotime(date('Y-m-d 00:00:00')) : strtotime($params['starttime']);
	// $params['endtime'] = empty($params['endtime'])? strtotime(date('Y-m-d 23:59:59')) : strtotime($params['endtime']);
	$current = empty($params['page'])? 1 : $params['page'];
	$params['start'] = ($current-1)*20;
	$query = http_build_query($params);
	$logs = $this->rest->get("v1/money/offline/recharge/{$user['userinfo']->id}?{$query}");
	$page = new Page($logs->total,$current);
	$type = ['alipay'=>'支付宝', 'bank'=>"网银"];
	$assign = ['logs'=>$logs,'type'=>$type,'page'=>$page,'params'=>$params];
	$this->renderer->render($response, 'money/offline-log.html', $assign);
});

$app->get('/union/pay', function ($request, $response, $args) {
	$this->token->checkSignStatus();
    $user = $this->token->getSignInfo();
    $v_mid = '110180220003';
	$params = $request->getParams();
	$orderid =  date('Ymd').'-'.$v_mid.'-'.$user['userinfo']->id.date('His');
	$postdata = ['token'=>$user['token'],'orderid'=>$orderid,'money'=>$params['money'],'type'=>'unionpay'];
	$rst = $this->rest->post("v1/money/orderid/{$user['userinfo']->id}", $postdata);
	if(isset($rst->errno)){
		exit($rst->error);
	}
	//发送支付请求，跳转至网银进行支付
	$assign['token'] = $user['token'];
	$assign['key'] = 'J4ezfWoEMRKXPKetyw5aHDmialnrD5ul';
	$assign['v_mid'] = $v_mid;
	$assign['v_oid'] =$orderid;
	$assign['v_amount'] = $params['money'];
	$assign['v_moneytype'] = 'CNY';
	$assign['v_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/union/pay/receive';
	$assign['remark2'] = '[url:=http://'.$_SERVER['HTTP_HOST'].'/union/pay/autoreceive]';
	$assign['v_md5info'] = strtoupper(md5($assign['v_amount'].$assign['v_moneytype'].$assign['v_oid'].$assign['v_mid'].$assign['v_url'].$assign['key']));
	$this->renderer->render($response, 'money/unionpay.html', $assign);
});

$app->any('/union/pay/receive', function ($request, $response, $args) {
	$key='J4ezfWoEMRKXPKetyw5aHDmialnrD5ul';
	$v_oid       = trim($_REQUEST['v_oid']);       // 商户发送的v_oid定单编号   
	$v_pmode     = trim($_REQUEST['v_pmode']);    // 支付方式（字符串）   
	$v_pstatus   = trim($_REQUEST['v_pstatus']);   //  支付状态 ：20（支付成功）；30（支付失败）
	$v_pstring   = trim($_REQUEST['v_pstring']);   // 支付结果信息 ： 支付完成（当v_pstatus=20时）；失败原因（当v_pstatus=30时,字符串）； 
	$v_amount    = trim($_REQUEST['v_amount']);     // 订单实际支付金额
	$v_moneytype = trim($_REQUEST['v_moneytype']); //订单实际支付币种    
	$token     = trim($_REQUEST['remark1' ]);      //备注字段1
	$remark2     = trim($_REQUEST['remark2' ]);     //备注字段2
	$v_md5str    = trim($_REQUEST['v_md5str' ]);   //拼凑后的MD5校验值  
	$md5string=strtoupper(md5($v_oid.$v_pstatus.$v_amount.$v_moneytype.$key));

	/**
	 * 判断返回信息，如果支付成功，并且支付结果可信，则做进一步的处理
	 */
	if ($v_md5str==$md5string){
		if($v_pstatus=="20"){
			//支付成功，可进行逻辑处理！
			//商户系统的逻辑处理（例如判断金额，判断支付状态，更新订单状态等等）......
			$putdata = ['token'=>$token,'orderid'=>$v_oid,'descr'=>"银联订单号：{$v_oid}"];
    		$data = $this->rest->put('v1/money/recharge', $putdata);
			if(isset($data->errno)){
				if($data->errno == '201'){
					$assign['result'] = '订单已经完成';
				}else{
	    			$assign['result'] = $data->error;
				}
	    		$assign['style'] = "color:red";
    			
    		}else{
    			$assign['result'] = '您已成功支付';
    			$assign['style'] = "color:rgb(106, 199, 29)";
    		}
		}else{
			$assign['result'] = "支付失败";
			$assign['style'] = "color:red";
		}
	}else{
		$assign['result'] = "校验失败,数据可疑";
		$assign['style'] = "color:red";
	}
	$this->renderer->render($response, 'money/pay-success.html', $assign);
});


$app->any('/union/pay/autoreceive', function ($request, $response, $args) {
	$key='J4ezfWoEMRKXPKetyw5aHDmialnrD5ul';
	$v_oid       = trim($_REQUEST['v_oid']);
	$v_pmode     = trim($_REQUEST['v_pmode']);
	$v_pstatus   = trim($_REQUEST['v_pstatus']);
	$v_pstring   = trim($_REQUEST['v_pstring']);
	$v_amount    = trim($_REQUEST['v_amount']);
	$v_moneytype = trim($_REQUEST['v_moneytype']);
	$token     = trim($_REQUEST['remark1']);
	$remark2     = trim($_REQUEST['remark2']);
	$v_md5str    = trim($_REQUEST['v_md5str']);
	$md5string=strtoupper(md5($v_oid.$v_pstatus.$v_amount.$v_moneytype.$key)); //拼凑加密串
	if ($v_md5str!=$md5string || $v_pstatus!="20"){
		   exit("error");
	}
	$putdata = ['token'=>$token,'orderid'=>$v_oid,'descr'=>"银联订单号：{$v_oid}"];
	$data = $this->rest->put('v1/money/recharge', $putdata);
	if(isset($data->errno)){
		echo "error";
	}else{
		echo "ok";
	}
});