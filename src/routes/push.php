<?php
$app->get('/push/platform', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $assign['lps_url'] = $this->get('settings')['lps_url'];
    $signInfo = $this->token->getSignInfo();
    $assign['user_level'] = $signInfo['userinfo']->level;

    $list = $this->rest->get("v1/push/server/list/{$signInfo['userinfo']->id}?token={$signInfo['token']}");
    $assign['list'] = $list;
    $this->renderer->render($response, 'push/platform.html', $assign);
});

$app->post('/push/add/server', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $signInfo = $this->token->getSignInfo();
    $params = $request->getParams();
    $params['token'] = $signInfo['token'];
    $params['user_id'] = $signInfo['userinfo']->id;
    $rst = $this->rest->post("v1/push/server?token={$signInfo['token']}", $params);
    if(isset($rst->errno)){
        echo '<script>alert("'.$rst->error.'");window.location.href="/push/platform"</script>';
    }else{
        return $response->withStatus(301)->withHeader('Location', '/push/platform');
    }
});

$app->get('/push/del/server/{server_id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $signInfo = $this->token->getSignInfo();
    $rst = $this->rest->delete("v1/push/del/server/{$args['server_id']}?token={$signInfo['token']}&user_id={$signInfo['userinfo']->id}");
    if(isset($rst->errno)){
        echo '<script>alert("'.$rst->error.'");window.location.href="/push/platform"</script>';
    }else{
        return $response->withStatus(301)->withHeader('Location', '/push/platform');
    }
});

$app->post('/push/add/url', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $signInfo = $this->token->getSignInfo();
    $post = $request->getParams();
    $push_urls = [];
    foreach($post['url'] as $key=>$value){
        if(!empty($value)){
            $push_urls['url'.$key] = $value;
        }
    }
    $params['server_id'] = $post['server_id'];
    $params['urls'] = json_encode($push_urls);
    $params['token'] = $signInfo['token'];
    $params['user_id'] = $signInfo['userinfo']->id;
    $rst = $this->rest->put("v1/push/edit/url?token={$signInfo['token']}", $params);
    if(isset($rst->errno)){
        echo '<script>alert("'.$rst->error.'");window.location.href="/push/platform"</script>';
    }else{
        return $response->withStatus(301)->withHeader('Location', '/push/platform');
    }
});

$app->post('/start/push/{server_id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $signInfo = $this->token->getSignInfo();
    $params['user_id'] = $signInfo['userinfo']->id;
    $params['token'] = $signInfo['token'];
    $rst = $this->rest->put("v1/push/start/{$args['server_id']}?token={$signInfo['token']}", $params);
    return json_encode($rst);
});

$app->post('/stop/push/{server_id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $signInfo = $this->token->getSignInfo();
    $params['user_id'] = $signInfo['userinfo']->id;
    $params['token'] = $signInfo['token'];
    $rst = $this->rest->put("v1/push/stop/{$args['server_id']}?token={$signInfo['token']}", $params);
    return json_encode($rst);
});


$app->get('/push/get/time/{server_id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $signInfo = $this->token->getSignInfo();
    $params['user_id'] = $signInfo['userinfo']->id;
    $params['token'] = $signInfo['token'];
    $query = http_build_query($params);
    $rst = $this->rest->get("v1/push/get/time/{$args['server_id']}?{$query}");
    return json_encode($rst);
});

$app->get('/push/status/{server_id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $rst = $this->rest->get("v1/push/status/{$args['server_id']}");
    if(!isset($rst->errno)){
        if ($rst->data) {
            return json_encode($rst->data);
        }else{
            return '{}';
        }
    }
});