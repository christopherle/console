<?php
// SSO

// 其他站点登陆授权调用页面
// 请求格式：	http://127.0.0.1:8888/sso/authority?callback=urlencode(url)
// 回应格式：	http://127.0.0.1:8080/login?id=101&token=f398e177a40ae93331c2d3c0dda42b2b
$app->get('/sso/authority', function($request, $response,$args){
	// 参数检查
	$post = $request->getParams();
	if (empty($post['callback'])) {
		echo 'format error';
		return;
	}

	$callback = urldecode($post['callback']);
	$signinfo = $this->token->getSignInfo();
	$userinfo = $signinfo['userinfo'];

	// 如果用户没有登陆跳转到登陆页面
	if (empty($signinfo['token'])) {
		return $response->withStatus(301)->withHeader('Location', "/login?callback={$callback}");
	}
	$resdata = ['id' => $userinfo->id, 'token' => $signinfo['token']];

	$query = http_build_query($resdata);

	return $response->withStatus(301)->withHeader('Location', "{$callback}?{$query}");
});

//错误日志
$app->post('/logs/add', function ($request, $response, $args) {
    
    $post = $request->getParams();
   
    
    $rst = $this->rest->post("v1/logs/add", $post);
    
});

$app->delete('/dms/history/{topic}/{id}', function ($request, $response, $args) {
    
    $rst = $this->rest->delete("v1/service/dms/history/{$args['topic']}/{$args['id']}");
    return json_encode($rst);
    
});

$app->get('/dms/history/{topic}/{start}', function ($request, $response, $args) {
    
    $rst = $this->rest->get("v1/service/dms/history/{$args['topic']}/{$args['start']}");
    return json_encode($rst);
    
});

