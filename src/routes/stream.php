<?php
//直播分享信息
$app->get('/stream/share/{id}', function ($request, $response, $args) {
	$this->token->checkSignStatus();
    $userinfo = $this->token->getSignInfo();
    $item = $this->rest->get("v1/streams/{$args['id']}");
    $url = "//{$_SERVER['HTTP_HOST']}/watch/{$item->id}";
    $settings = $this->get('settings');
    $static = $settings['storage_baseurl'];
    $data = ['url' => $url, 'item' => $item, 'static' => $static];
	$settings = $this->get('settings');
	$data['player_url'] = $settings['player_url'];
    $this->renderer->render($response, 'stream/share.html', $data);
});

//直播管理
$app->get('/stream/manager/{id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $userinfo = $this->token->getSignInfo();
    $item = $this->rest->get("v1/streams/{$args['id']}");
    $dvrs7day = $this->rest->get("v1/service/dvrs/7day?stream={$item->name}");
    $watch_url = http()."//{$_SERVER['HTTP_HOST']}/watch/{$item->id}";
    $preview_url = $watch_url.'/preview?token='.$userinfo['token'];
    $balance = $this->rest->get("v1/money/balance/{$userinfo['userinfo']->id}?token={$userinfo['token']}");

    $assign = ['item' => $item, 'watch_url' => $watch_url, 'user' => $userinfo, 'dvrs7day' => $dvrs7day, 'preview_url' => $preview_url];
    $assign['balance'] = isset($balance->data)? $balance->data : '0.00';
    $this->renderer->render($response, 'stream/manager.html', $assign);
});

//永久存储列表
$app->get('/stream/dvrs', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $userinfo = $this->token->getSignInfo();
	$dvrs = $this->rest->get("v1/service/dvrs?token=".$userinfo['token']);
    $data = ['list' => $dvrs->list];
    $this->renderer->render($response, 'stream/dvrs.html', $data);
});

//同步永久存储
$app->post('/stream/dvrs', function ($request, $response, $args) {
    $this->token->checkSignStatus();
	$post = $request->getParams();
    $userinfo = $this->token->getSignInfo();
    if($userinfo['userinfo']->level < 2){
		return '{"errno":101,"error":"需升级到专业用户才可以保存。"}';
	}
	$rst = $this->rest->post("v1/service/dvrs?token".$userinfo['token'],['url'=>$post['url'],'token'=>$userinfo['token']]);
	return json_encode($rst);
});

//删除永久存储
$app->delete('/stream/dvrs', function ($request, $response, $args) {
    $this->token->checkSignStatus();
	$post = $request->getParams();
    $userinfo = $this->token->getSignInfo();
	//todo 检查是否添加到精彩回放
	$rst = $this->rest->delete("v1/service/dvrs?url={$post['url']}&token=".$userinfo['token']);
	return json_encode($rst);
});

//添加拉流
$app->post('/stream/pullurl', function ($request, $response, $args) {
    $this->token->checkSignStatus();
	$post = $request->getParams();
    $userinfo = $this->token->getSignInfo();
	$post['token'] = $userinfo['token'];
	$rst = $this->rest->post("/v1/service/stream/pullurl",$post);
	return json_encode($rst);
});

//停止拉流
$app->delete('/stream/pullurl', function ($request, $response, $args) {
    $this->token->checkSignStatus();
	$post = $request->getParams();
    $userinfo = $this->token->getSignInfo();
	$post['token'] = $userinfo['token'];
	$query = http_build_query($post);
	$rst = $this->rest->delete("/v1/service/stream/pullurl?{$query}");
	return json_encode($rst);
});

//频道列表
$app->get('/stream/my', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $userinfo = $this->token->getSignInfo();
    $list = $this->rest->get("v1/streams?token={$userinfo['token']}");
    $data = ['list' => $list];
	$level = [1=>'普通用户',2=>'专业用户',3=>'企业用户'];
	$data['levelname'] = $level[$userinfo['userinfo']->level];
	$data['maxcreatenum'] = getMaxCreate($userinfo['userinfo']->level);
    $this->renderer->render($response, 'stream/list.html', $data);
});

//断开直播
$app->get('/stream/offline/{id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $userinfo = $this->token->getSignInfo();
    $list = $this->rest->put("v1/service/stream/status/{$args['id']}", ['token' => $userinfo['token']]);
    if (isset($list->errno)) {
        throw new Exception($list->error, 101);
    }
    $this->renderer->render($response, 'success.html', ['back' => '/stream/my', 'msg' => '成功断开直播，如需再次直播请重新设置推流地址。']);
});

//频道统计
$app->get('/stream/count/{id}', function ($request, $response, $args) {
 //    $this->token->checkSignStatus();
	$params = $request->getParams();
    $stream = $this->rest->get("v1/streams/{$args['id']}");
    if (isset($stream->errno)) {
        throw new Exception($stream->error, 101);
    }
	if(empty($params['starttime'])){
		$params['starttime'] = time();
	}else{
		$params['starttime'] = strtotime($params['starttime']);
	}

    $assign['params'] = $params;
	

    $this->token->checkSignStatus();
    $signinfo = $this->token->getSignInfo();
    $date = date('Y-m-d H:i:s',$params['starttime']);
    $arr = ['pv'=>'streams_watch_count_pv_hour', 'ip'=>'streams_watch_count_ip_hour', 'uv'=>'streams_watch_count_uv_hour', 'time'=>'streams_watch_count_time_hour'];
    $params = ['stream_id'=>$args['id'], 'date'=>$date,'token'=>$signinfo['token']];
    $query = http_build_query($params);
    $pv = $this->rest->get("/v1/streams/watch/count/hour?{$query}&type={$arr['pv']}");
    $uv = $this->rest->get("/v1/streams/watch/count/hour?{$query}&type={$arr['uv']}");
    $ip = $this->rest->get("/v1/streams/watch/count/hour?{$query}&type={$arr['ip']}");
    $time = $this->rest->get("/v1/streams/watch/count/hour?{$query}&type={$arr['time']}");
    $assign['pv'] = tendencyChart($pv);
    $assign['uv'] = tendencyChart($uv);
    $assign['ip'] = tendencyChart($ip);
    $assign['time'] = tendencyChart($time);
    $spread = $this->rest->get("/v1/streams/watch/city/hour/{$args['id']}?token={$signinfo['token']}&date={$date}");
    $assign['ipMax'] = isset($ip[0]) ? $ip[0]->num : 0;

    $assign['sort'] = (array)$spread->province;
    $province = ['北京'=>0,'天津'=>0,'上海'=>0,'重庆'=>0,'河北'=>0,'河南'=>0,'云南'=>0,'辽宁'=>0,'黑龙江'=>0,'湖南'=>0,'安徽'=>0,'山东'=>0,'新疆'=>0,'江苏'=>0,'浙江'=>0,'江西'=>0,'湖北'=>0,'广西'=>0,'甘肃'=>0,'山西'=>0,'内蒙古'=>0,'陕西'=>0,'吉林'=>0,'福建'=>0,'贵州'=>0,'广东'=>0,'青海'=>0,'西藏'=>0,'四川'=>0,'宁夏'=>0,'海南'=>0,'台湾'=>0,'香港'=>0,'澳门'=>0,'南海诸岛'=>0];
    foreach($spread->province as $item){
        $province[$item->province] = $item->num;
    }
	if(empty($spread->device)){
		$assign["device"] = (object)array('windows'=>0,'mac'=>0,'ios'=>0,'android'=>0,'other'=>0);
	}else{
		$assign["device"] = pieChart($spread->device,'device');
	}
	if(empty($spread->app)){
		$assign["app"] = (object)array('wechat'=>0,'chrome'=>0,'safari'=>0,'webkit'=>0,'other'=>0);
	}else{
		$assign["app"] = pieChart($spread->app, 'app');
	}
    $assign['id'] = $args['id'];
    $assign['province'] = $province;
    $totalTime = sum($time)*60;
    $totalUv = sum($uv);
    $totalPv = sum($pv);
    $totalIp = sum($ip);
    $assign['sum'] = ['ip'=>$totalIp, 'pv'=>$totalPv, 'uv'=>$totalUv, 'time'=>secFormat($totalTime), 'aveTime'=>$totalUv==0?secFormat(0):secFormat($totalTime/$totalUv)];
    $this->renderer->render($response, 'stream/count.html', $assign);
});
function tendencyChart($data){
    $arr = [];
    foreach ($data as $item) {
        $key = (int)substr($item->created_at, 8);
        $arr[$key] = $item->num;
    }
    for($i=0;$i<=23;$i++){
        if(!isset($arr[$i])) $arr[$i] = 0;
    }
    ksort($arr);
    return $arr;
}
function pieChart($data,$type){
    $arr = [];
    foreach($data as $item){
        $arr[$item->$type] = $item->num;
    }
    return $arr;
}
function sum($data){
    $num = 0;
    foreach ($data as $value) {
       $num += $value->num;
    }
    return $num;
} 

function secFormat($time){
    $h=$i=$s = '00';
    if($time>=3600){
        $h = addFormatTimeZero(intval($time/3600));
        $time %= 3600;
    }
    if($time>=60){
        $i = addFormatTimeZero(intval($time/60));
        $time %= 60;
    }
    $s = addFormatTimeZero(intval($time));
    return $h.':'.$i.':'.$s;
}

function addFormatTimeZero($time){
    if($time<10) $time = '0'.$time;
    return $time;
}

// 频道用户观看流水记录
$app->get('/stream/logs/{id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
	$params = $request->getParams();
    if(empty($params['date'])){
        $params['starttime'] = strtotime(date('Y-m-d 00:00:00'));
        $params['endtime'] = strtotime(date('Y-m-d 29:59:59'));
    }else{
        $params['starttime'] = strtotime($params['date']);
        $params['endtime'] = strtotime($params['date'])+86399;
    }
    unset($params['date']);
	$current = empty($params['page'])? 1 : $params['page'];
	$params['start'] = ($current-1)*20;
	$query = http_build_query($params);
    $logs = $this->rest->get("v1/streams/logs/{$args['id']}?{$query}");
	$page = new Page($logs->total,$current);
	$assign = ['logs'=>$logs,'page'=>$page,'params'=>$params,'stream_id'=>$args['id']];
    $this->renderer->render($response, 'stream/logs.html', $assign);
});

//删除频道
$app->get('/stream/delete/{id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $userinfo = $this->token->getSignInfo();
    $list = $this->rest->delete("v1/streams/{$args['id']}?token={$userinfo['token']}");
    if (isset($list->errno)) {
        throw new Exception($list->error, 101);
    } else {
        return $response->withStatus(301)->withHeader('Location', '/stream/my');
    }
});

//新建频道
$app->map(['GET', 'POST'], '/stream/create', function ($request, $response, $args) {
    $this->token->checkSignStatus();

    $userinfo = $this->token->getSignInfo();
    $id = $userinfo['userinfo']->id;
    $post = $request->getParams();
    $assign['id'] = $id;
    if (empty($post)) {
        $assign['post'] = ['name' => '', 'alias' => '', 'cover' => "http://{$_SERVER['HTTP_HOST']}/static/console/img/cover.jpg", 'announce' => '', 'start_live' => time(), 'stop_live' => time()+7200];
    } else {
		$post['start_live'] = strtotime($post['start_live']);
		$post['stop_live'] = strtotime($post['stop_live']);
		$assign['post'] = $post;
        $userinfo = $this->token->getSignInfo();
        $post['token'] = $userinfo['token'];
        $post['name'] = $id.substr(md5(time().uniqid()), rand(0, 16), 16 - strlen($id));
		$post['cover'] = "http://{$_SERVER['HTTP_HOST']}/static/console/img/cover.jpg";
		$post['h5_cover'] = "http://{$_SERVER['HTTP_HOST']}/static/console/img/h5_cover.jpg";
        $data = $this->rest->post('v1/streams', $post);
        return json_encode($data);
        if (!isset($data->errno)) {
            // $this->renderer->render($response, 'success.html', ['back' => '/stream/my', 'msg' => '成功创建直播。']);

            return json_encode(['rst'=>'1','tip'=>'成功创建直播']);
        } else {
            //$assign['error'] = $data->error;
			// $this->renderer->render($response, 'error.html', ['back' => '/stream/my', 'msg' => $data->error]);
			
			return json_encode(['rst'=>'0','tip'=>$data->error]);
		}
    }
	if(empty($assign['post']['cover'])){
		$assign['post']['cover'] = "http://{$_SERVER['HTTP_HOST']}/static/console/img/cover.jpg";
	}

	$settings = $this->get('settings');
	$assign['uploadurl'] = $settings['storage_baseurl'].'/addimage.php?id='.$userinfo['userinfo']->id.'&token='.$settings['storage_token'];
    // $this->renderer->render($response, 'stream/create.html', $assign);
});

//修改频道
$app->map(['GET', 'POST'], '/stream/update/{id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $userinfo = $this->token->getSignInfo();
    $post = $request->getParams();
    $assign = [];
    if (empty($post)) {
        $assign['post'] = (array) $this->rest->get("v1/streams/{$args['id']}");
        $assign['id'] = $userinfo['userinfo']->id;
    } else {
		$post['start_live'] = strtotime($post['start_live']);
		$post['stop_live'] = strtotime($post['stop_live']);
		$assign['post'] = $post;
        $userinfo = $this->token->getSignInfo();
        $post['id'] = $args['id'];
        $post['token'] = $userinfo['token'];
        $id = $userinfo['userinfo']->id;
        $data = $this->rest->put('v1/streams', $post);
        if (!isset($data->errno)) {
            // $this->renderer->render($response, 'success.html', ['back' => '/stream/my', 'msg' => '直播信息已更新。']);

            return json_encode(['rst'=>'1','tip'=>'成功修改直播']);
        } else {
            return json_encode(['rst'=>'0','tip'=>$data->error]);
        }
    }
	if(empty($assign['post']['cover'])){
		$assign['post']['cover'] = "http://{$_SERVER['HTTP_HOST']}/static/console/img/cover.jpg";
	}
	$settings = $this->get('settings');
	$assign['uploadurl'] = $settings['storage_baseurl'].'/addimage.php?id='.$userinfo['userinfo']->id.'&token='.$settings['storage_token'];
    $this->renderer->render($response, 'stream/update.html', $assign);
});

//选择直播主题
$app->map(['GET', 'POST'], '/stream/themes/{id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    //初始化信息
    $post = $request->getParams();
    $assign = [];
    $userinfo = $this->token->getSignInfo();
    $custom = (array) $this->rest->get("v1/streams/custom/{$args['id']}");
    if (isset($custom['errno'])) {
        $custom = ['stream_id' => $args['id'], 'template' => 'simple', 'custom_config' => [], 'user_id' => $userinfo['userinfo']->id];
    }
    
    //保存主题信息
    if (!empty($post)) {
        if (empty($post['custom_config'])) {
            $post['custom_config'] = $custom['custom_config'];
        }
        $custom = array_merge($custom, $post);
        $custom['custom_config'] = json_encode($custom['custom_config']);
        $custom['token'] = $userinfo['token'];
        $data = $this->rest->post("v1/streams/custom/{$args['id']}", $custom);
        if (!isset($data->errno)) {
            exit('{"rst":"success"}');
        } else {
            exit('{"rst":"fail"}');
        }
    }
    $level = $userinfo['userinfo']->level;
    //遍历所有主题
    $dirs = glob(dirname(__FILE__).'/../../public/themes/liveview/*');
    $themes = [];
    foreach ($dirs as $dir) {
        if (is_dir($dir)) {
            $info = json_decode(trim(file_get_contents($dir.'/info.json')));
            if(isset($info->level)){
                $levels = explode(',', $info->level);
                if(in_array($level, $levels)){
                    $name = explode('liveview', $dir);
                    $info->name = trim($name[1], '/');
                    $themes[] = $info;
                }
            }else{
                $name = explode('liveview', $dir);
                $info->name = trim($name[1], '/');
                $themes[] = $info;
            }
        }
    }
    $assign['themes'] = $themes;
    $assign['custom'] = $custom;

    $this->renderer->render($response, 'stream/themes.html', $assign);
});

//设置主题自定义项
$app->map(['GET', 'POST'], '/stream/custom/{id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    //初始化信息
    $post = $request->getParams();
    $assign = [];
    $userinfo = $this->token->getSignInfo();
    $assign['userlevel'] = $userinfo['userinfo']->level;
    // var_dump($assign['userlevel']);
    // exit;
    $custom = (array) $this->rest->get("v1/streams/custom/{$args['id']}");
    if (isset($custom['errno'])) {
        $custom = ['token'=>$userinfo['token'],'stream_id' => $args['id'], 'template' => 'simple', 'custom_config' => [], 'user_id' => $userinfo['userinfo']->id];
    } else {
        $custom['custom_config'] = (array)$custom['custom_config'];
		$custom['token'] = $userinfo['token'];
		$assign['post'] = $custom['custom_config'];
    }
    $assign['custom'] = $custom;
    //保存主题信息
    if (!empty($post)) {

        if(isset($post['uplogo_hide']) && $post['uplogo_hide'] == 'on'){
            $post['uplogo_hide'] = 0;
        }else{
            $post['uplogo_hide'] = 1;
        }
        if(isset($post['downlogo_hide'])){
            if($post['downlogo_hide'] == 'on'){
                $post['downlogo_hide'] = 0;
            }else{
                $post['downlogo_hide'] = 1;
            }
        }else{
            if($userinfo['userinfo']->level == 1){
                $post['downlogo_hide'] = 0;
            }else{
                $post['downlogo_hide'] = 1;
            }
        }
        // if(isset($post['downlogo_hide']) && $post['downlogo_hide'] == 'on'){
        //     $post['downlogo_hide'] = 0;
        // }else{
        //     $post['downlogo_hide'] = 1;
        // }
        $post['logo'] = '';
        $post['logo_status'] = '';
        if(isset($post['bottom_txt'])) $post['bottom_txt'] = htmlspecialchars($post['bottom_txt'], ENT_QUOTES);
        if(empty($post['about_title'][0]) && !empty($post['about_title'][1])){
            $post['about_title'][0] = $post['about_title'][1];
            $post['about_title'][1] = '';
            $post['about_descr'][0] = $post['about_descr'][1];
            $post['about_descr'][1] = '';
        }
        //修改封面和H5封面
        $params = ['token'=> $userinfo['token'], 'id'=>$args['id'],'cover'=>$post['cover'],'h5_cover'=>$post['h5_cover']];
        $list = $this->rest->put("v1/streams/cover/url?token={$userinfo['token']}",$params);

        foreach($post['about_title'] as $k=>$v){
            if(empty($v)){
                unset($post['about_title'][$k],$post['about_descr'][$k]);
            }
        }
        $custom['custom_config'] = json_encode($post);
        $data = $this->rest->post("v1/streams/custom/{$args['id']}", $custom);
        if (!isset($data->errno)) {
            $this->renderer->render($response, 'success.html', ['msg' => '保存成功，即将返回频道列表。']);
        } else {
            $this->renderer->render($response, 'error.html', ['msg' => '保存失败']);
        }
        return;
    }
    $this->kvstorage->del('info'.$args['id']);

    $assign['theme'] = $custom['template'];


	if(empty($assign['post']['page_header'])) $assign['post']['page_header'] = '';
	if(empty($assign['post']['ad_img'])) $assign['post']['ad_img'] = '';
	if(empty($assign['post']['ad_url'])) $assign['post']['ad_url'] = '';
	if(empty($assign['post']['about_title'])) $assign['post']['about_title'][] = '';
	if(empty($assign['post']['about_descr'])) $assign['post']['about_descr'][] = '';
    if(empty($assign['post']['cover'])) $assign['post']['cover'] = '';
    if(empty($assign['post']['h5_cover'])) $assign['post']['h5_cover'] = '';
	if(empty($assign['post']['uplogo_src'])) $assign['post']['uplogo_src'] = '';
    if(empty($assign['post']['downlogo_src'])) $assign['post']['downlogo_src'] = '';
    if(!isset($assign['post']['uplogo_hide'])) $assign['post']['uplogo_hide'] = 1;
    if(!isset($assign['post']['downlogo_hide'])) $assign['post']['downlogo_hide'] = 1;

    if(empty($assign['post']['uplogo_ratio'])) $assign['post']['uplogo_ratio'] = 0.25;
    if(empty($assign['post']['downlogo_ratio'])) $assign['post']['downlogo_ratio'] = 0.25;
    if(empty($assign['post']['uplogo_rotate'])) $assign['post']['uplogo_rotate'] = 0;
    if(empty($assign['post']['downlogo_rotate'])) $assign['post']['downlogo_rotate'] = 0;


    if(empty($assign['post']['bottom_txt'])) $assign['post']['bottom_txt'] = '';
	$settings = $this->get('settings');
	$assign['uploadurl'] = $settings['storage_baseurl'].'/addimage.php?id='.$userinfo['userinfo']->id.'&token='.$settings['storage_token'];
    $this->renderer->render($response, 'stream/custom.html', $assign);
});

$app->get('/stream/download/excel/{stream_id}', function ($request, $response, $args) {
    $this->token->checkSignStatus();
    $signinfo = $this->token->getSignInfo();
    $params = $request->getParams();
    $params['token'] = $signinfo['token'];
    $query = http_build_query($params);
    $rst = $this->rest->get("/v1/streams/excel/data/{$args['stream_id']}?{$query}");
    $type = ['pano_vod'=>'点播', 'pano_live'=> '直播', 'pull_live'=>'拉流'];
    // var_dump($rst);
    foreach($rst as $v){
        unset($v->stream_id, $v->user_id, $v->watch_time, $v->ispv, $v->id);
        // var_dump($v);
        $v->created_at = date('Y-m-d H:i', $v->created_at);
        $v->type = isset($type[$v->type]) ? $type[$v->type] : $v->type;
    }
    $head = json_decode(json_encode(['type'=>'类型', 'client_ip'=>'IP', 'province'=>'省份', 'city'=>'城市', 'device'=>'设备', 'app'=>'APP', 'created_at'=>'时间']));
    array_unshift($rst, $head);

    DownloadExcel::download($rst, 'sheet1', $params['date']);
});
