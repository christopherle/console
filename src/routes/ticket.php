<?php
//添加工单
$app->map(['GET', 'POST'], '/ticket/add', function ($request, $response, $args) {
    $assign = [];
    $user = $this->token->getSignInfo();
    $post = $request->getParams();
    if(!empty($post)){
        $params['token'] = $user['token'];
        $query = http_build_query($params);
        $post['token'] = $user['token'];
        $rst = $this->rest->post("v1/ticket/add/{$user['userinfo']->id}?{$query}", $post);
        if (!isset($rst->errno)) {
            // $this->renderer->render($response,'success.html',['back'=>'/ticket/list','msg'=>'提交成功']);
            $return = ['rst'=>1,'tip'=>'添加成功'];
            return json_encode($return);
        } else {
            // $assign['error'] = $rst->error;
            // $this->renderer->render($response,'error.html',['back'=>'/ticket/list','msg'=>$rst->error]);
            $return = ['rst'=>0,'tip'=>$rst->error];
            return json_encode($return);
        }
    }
    // $this->renderer->render($response, 'ticket/add.html', $assign);
});

//我的工单列表
$app->get('/ticket/list', function ($request, $response, $args) {
    $user = $this->token->getSignInfo();
    $get = $request->getParams();
    $wd = isset($get['wd']) ? $get['wd'] : '';
    $params['page'] = empty($get['page']) ?  1: $get['page'];
    $params['wd'] = $wd;
    $params['token'] = $user['token'];
    $query = http_build_query($params);
    $rst = $this->rest->get("v1/ticket/list?{$query}");
    $assign['list'] = $rst->tickets;
    $assign['wd'] = $wd;
    
    $show = $params['page'];
    $page = new Page($rst->num, $show);
    $assign['page'] = $page;
    $this->renderer->render($response, 'ticket/list.html', $assign);
});

//工单详情
$app->get('/ticket/detail/{ticket_id}', function ($request, $response, $args) {
    $ticket_id = $args['ticket_id'];
    $user = $this->token->getSignInfo();
    $params['token'] = $user['token'];
    $query = http_build_query($params);
    $rst = $this->rest->get("v1/ticket/detail/{$ticket_id}?{$query}");
    $assign['item'] = $rst;
    $this->renderer->render($response, 'ticket/detail.html', $assign);
});

//工单回复
$app->map(['GET', 'POST'], '/ticket/reply', function ($request, $response, $args) {
    $user = $this->token->getSignInfo();
    $post = $request->getParams();
    $post['token'] = $user['token'];
    $params['token'] = $user['token'];
    $query = http_build_query($params);
    $rst = $this->rest->put("v1/ticket/reply/{$user['userinfo']->id}?{$query}", $post);
    echo json_encode($rst);
});