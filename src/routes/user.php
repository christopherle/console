<?php
//注册
$app->map(['GET', 'POST'], '/signup', function ($request, $response, $args) {
    $post = $request->getParams();
    $assign = ['post'=>$post];
    if(empty($post)){
        $assign['post'] = ['email' => '', 'mobile' => '','code'=>''];
    }else{
		$query = ['email'=>$post['email'],'mobile'=>$post['mobile'],'mobile_active'=>1,'password'=>$post['password'],'code'=>$post['code']];
		$data = $this->rest->post('v1/users', $query);
		if (isset($data->errno)) {
			$assign['error'] = json_decode($data->error, true);
		}else{
			$this->renderer->render($response,'success.html',['back'=>'/login','msg'=>'注册成功，即将返回登录页面。']);
			return;
		}
    }
    $this->renderer->render($response, 'user/register.html', $assign);
});

// 实名认证
$app->map(['GET', 'POST'],'/user/realname', function($request, $response, $args) {
	$assign = [];
	$signinfo = $this->token->getSignInfo();
	$post = $request->getParams();
	$assign['post'] = $this->rest->get("v1/users/realname/{$signinfo['userinfo']->id}?token={$signinfo['token']}");
	if(!empty($post)){
		$query = ['token'=>$signinfo['token'],'user_id'=>$signinfo['userinfo']->id,'type'=>$post['utype'],'idcode'=>$post['idNumber'],'ida'=>$post['id1'],'idb'=>$post['id2'],'company'=>$post['company'],'company_id'=>$post['businessLicence'],'company_code'=>$post['organization'],'license'=>$post['businessFile'],'status'=>0];
		$data = $this->rest->post("v1/users/realname/{$signinfo['userinfo']->id}", $query);
		if(!isset($data->errno)){
			$this->renderer->render($response,'success.html',['back'=>'/user/realname','msg'=>'提交成功，请等待审核。']);
			return;
		}
	}
	
	$settings = $this->get('settings');
	$assign['uploadurl'] = $settings['storage_baseurl'].'/addimage.php?id='.$signinfo['userinfo']->id.'&token='.$settings['storage_token'];
    $this->renderer->render($response, 'user/realname.html',$assign);
});

$app->get('/user/realname/cancel', function($request, $response, $args) {
	$signinfo = $this->token->getSignInfo();
	$post = $request->getParams();
	$rst = $this->rest->delete("v1/users/realname/{$post['id']}?token={$signinfo['token']}",$query);
	if(isset($rst->errno)){
		exit('重置失败');
	}else{
		return $response->withStatus(301)->withHeader('Location', '/user/realname');
	}
});

// 发送验证码
$app->post('/user/regcode', function($request, $response, $args) {
    $post = $request->getParams();
	if(empty($post['imgcode']) || strtolower($post['imgcode']) != strtolower($_SESSION['captcha'])){
		return json_encode(['errno'=>101,'error'=>'图片验证码错误']);
	}
    $email = $this->rest->get("/v1/users/check/email?email=".$post['email']);
    if(isset($email->errno)){
        return json_encode($email);
    }
    $query = ['username'=>$post['username']];
    return json_encode($this->rest->post('v1/users/regcode', $query));
});
// 发送验证码
$app->post('/user/wechat/regcode', function($request, $response, $args) {
    $post = $request->getParams();
    if(empty($post['imgcode']) || strtolower($post['imgcode']) != strtolower($_SESSION['captcha'])){
        return json_encode(['errno'=>101,'error'=>'图片验证码错误']);
    }
    $query = ['username'=>$post['username']];
    return json_encode($this->rest->post('v1/users/regcode', $query));
});
//登录
$app->map(['GET', 'POST'], '/login', function ($request, $response, $args) {
    $post = $request->getParams();
	$callback = isset($post['callback']) ? $post['callback'] : '';
	unset($post['callback']);
    $assign = [];

    $settings = $this->get('settings');
    $assign['player_url'] = $settings['player_url'];
    if(empty($post)){
        $assign['post'] = ['username' => ''];
        // var_dump($assign['player_url']);
        // exit;
    }else{
        $assign['post'] = $post;
        $data = $this->rest->put('v1/users', $post);
        if ( ! isset($data->errno)) {
			$this->token->update((array) $data);

			if (! empty($callback)) {
				return $response->withStatus(301)->withHeader('Location', $callback);
			}

			return $response->withStatus(301)->withHeader('Location', '/console');
        }

        $assign['error'] = $data->error;
    }

	@$signinfo = $this->token->getSignInfo();
    // var_dump($signinfo);
    if(@$signinfo){
        $assign['is_login'] = 1;
    }else{
        $assign['is_login'] = 0;
    }
	$assign['callback'] = $callback;

	$this->renderer->render($response, 'user/login.html', $assign);
});

//退出
$app->get('/signout', function ($request, $response, $args) {
    $_SESSION = [];
	session_unset();
	session_destroy();
    return $response->withStatus(301)->withHeader('Location', '/');
});

// 发送验证码
$app->map(['POST'], '/user/forgotcode', function($request, $response, $args) {
    $post = $request->getParams();
    $signinfo = $this->token->getSignInfo();
    $query = ['username'=>$post['username']];
    return json_encode($this->rest->post('v1/users/findcode', $query));
});

//忘记密码
$app->map(['GET', 'POST'],'/forgot', function ($request, $response, $args) {
	$post = $request->getParams();
    $assign = [];
	if(!empty($post)){
		$data = $this->rest->get("v1/users/check/code/{$post['code']}/{$post['username']}");
        if(!isset($data->errno)){
            $_SESSION['forgot'] = json_encode(['username'=>$post['username'],'code'=>$post['code']]);
    		return $response->withStatus(301)->withHeader('Location', '/forgot2');
        }
        $assign['error'] = '验证码错误';
	}
    $this->renderer->render($response, 'user/forgot.html', $assign);
});

//重置密码
$app->map(['GET', 'POST'],'/forgot2', function ($request, $response, $args) {
	if(empty($_SESSION['forgot'])){
		return $response->withStatus(301)->withHeader('Location', '/forgot');
	}
	$post = $request->getParams();
	if(!empty($post)){
		$forgot = json_decode($_SESSION['forgot'],true);
		$query = ['username'=>$forgot['username'],'code'=>$forgot['code'],'password'=>$post['password']];
		$data = $this->rest->put('v1/users/password', $query);
		if ( ! isset($data->errno)) {
			$_SESSION['forgot'] = null;
			unset($_SESSION['forgot']);
			return $response->withStatus(301)->withHeader('Location', '/forgot3');
		} else {
			$this->renderer->render($response,'error.html',['back'=>'/forgot','msg'=>$data->error]);
			return;
		}
	}
    $this->renderer->render($response, 'user/forgot2.html');
});

//重置密码
$app->map(['GET', 'POST'],'/forgot3', function ($request, $response, $args) {
    $this->renderer->render($response, 'user/forgot3.html');
});

// 绑定邮箱
$app->map(['GET', 'POST'], '/user/email', function($request, $response, $args) {
    $signinfo = $this->token->getSignInfo();
	
	$post = $request->getParams();
	
	if(!empty($post)){
		$query = ['token'=>$signinfo['token'],'username'=>$post['username'],'code'=>$post['code']];
		$data = $this->rest->put('v1/users/bind',$query);
		if ( ! isset($data->errno)) {
            $signinfo['userinfo']->email_active = 1;
			$signinfo['userinfo']->email = $query['username'];
            $this->renderer->render($response,'success.html',['back'=>'/user/email','msg'=>'邮箱绑定成功，即将返回绑定页面']);
        } else {
            $this->renderer->render($response,'error.html',['back'=>'/user/email','msg'=>'邮箱绑定失败，即将返回绑定页面。']);
        }
	}else{
		$assign['userinfo'] = $signinfo['userinfo'];
		$this->renderer->render($response, 'user/email.html', $assign);
	}
});

// 绑定电话
$app->map(['GET', 'POST'], '/user/mobile', function($request, $response, $args) {
    $signinfo = $this->token->getSignInfo();
	$post = $request->getParams();
	if(!empty($post)){
		$query = ['token'=>$signinfo['token'],'username'=>$post['username'],'code'=>$post['code']];
		$data = $this->rest->put('v1/users/bind',$query);
		if ( ! isset($data->errno)) {
            $signinfo['userinfo']->mobile_active = 1;
			$signinfo['userinfo']->mobile = $query['username'];
            $this->renderer->render($response,'success.html',['back'=>'/user/mobile','msg'=>'手机绑定成功，即将返回绑定页面']);
        } else {
            $this->renderer->render($response,'error.html',['back'=>'/user/mobile','msg'=>'手机绑定失败，即将返回绑定页面。']);
        }
	}else{
		$assign['userinfo'] = $signinfo['userinfo'];
		$this->renderer->render($response, 'user/mobile.html', $assign);
	}
});

// 发送验证码
$app->map(['POST'], '/user/sendcode', function($request, $response, $args) {
    $post = $request->getParams();
    $signinfo = $this->token->getSignInfo();
    $query = ['token'=>$signinfo['token'],'username'=>$post['username']];
    return json_encode($this->rest->post('v1/users/bindcode', $query));
});

// 修改密码
$app->map(['POST', 'GET'], '/user/password/edit', function($request, $response, $args){
    $post = $request->getParams();
    $assign = [];
    if( ! empty($post)){
        $signinfo = $this->token->getSignInfo();
        $userinfo = (array)$signinfo['userinfo'];
        $post['username'] = $userinfo['username'];
        $post['token'] = $signinfo['token'];
        $data = $this->rest->put('v1/users/' . $userinfo['id'] .'/password', $post);
        if ( ! isset($data->errno)) {
            $this->renderer->render($response,'success.html',['back'=>'/welcome','msg'=>'密码修改成功，即将返回欢迎页面。']);
            return;
        } else {
            $assign['error'] = $data->error;
        }
    }
    $this->renderer->render($response, 'user/password.html', $assign);
});

// 修改资料
$app->map(['POST', 'GET'], '/user/info/edit', function($request, $response, $args){
    $post = $request->getParams();
	$signinfo = $this->token->getSignInfo();
    $assign = ['userinfo'=>$signinfo['userinfo']];
	$assign['user_info'] = $this->rest->get("v1/users/userinfo/{$signinfo['userinfo']->id}");
    if( ! empty($post)){
        $post['token'] = $signinfo['token'];
		$data = $this->rest->put("v1/users/info/{$signinfo['userinfo']->id}", $post);
        if ( ! isset($data->errno)) {
			$signinfo['userinfo']->realname = $post['realname'];
			$signinfo['userinfo']->company = $post['company'];
            $this->renderer->render($response,'success.html',['msg'=>'修改成功，即将返回欢迎页面。']);
            return;
        } else {
            $assign['error'] = json_decode($data->error, true);
        }
    }
    $this->renderer->render($response, 'user/info.html', $assign);
});

// 安全设置
$app->map(['POST', 'GET'], '/user/info/safe', function($request, $response, $args){
	$signinfo = $this->token->getSignInfo();
    $assign = ['process'=>100,'names'=>[0=>'非常危险',20=>'危险',40=>'一般',60=>'一般',80=>'安全',100=>'非常安全']];
	$userinfo = $this->rest->get("v1/users/userinfo/{$signinfo['userinfo']->id}");
	$info = 1;

    $realname = $this->rest->get("v1/users/realname/{$signinfo['userinfo']->id}?token={$signinfo['token']}");
    if(isset($realname->data) && $realname->data=='fail'){
        $is_realname = 0;
        $assign['process'] -= 20;
    }else{
        $is_realname = 1;
    }
	foreach($userinfo as $val){
		if(empty($val)){
			$assign['process'] -= 20;
			$info = 0;
			break;
		}
	}
	if(!$signinfo['userinfo']->email_active){
		$assign['process'] -= 20;
	}
	if(!$signinfo['userinfo']->mobile_active){
		$assign['process'] -= 20;
	}
	$assign['options'] = [
		'password'=>1,
		'email'=>$signinfo['userinfo']->email_active,
        'email_num'=>$signinfo['userinfo']->email,
		'mobile'=>$signinfo['userinfo']->mobile_active,
        'mobile_num'=>$signinfo['userinfo']->mobile,
		'userinfo'=>$info,
        'is_realname'=>$is_realname
	];
    //安全度
    $safe = $assign['process'];
    if($safe < 50){
        $assign['safe'] = ['safe_lv' =>1,'name'=>'低','descr'=>'存在风险，请完善资料'];
    }else if($safe >= 50 && $safe <= 80){
        $assign['safe'] = ['safe_lv' =>2,'name'=>'中','descr'=>'再接再励'];
    }else{
        $assign['safe'] = ['safe_lv' =>3,'name'=>'高','descr'=>'非常安全'];
    }
    $this->renderer->render($response, 'user/safe.html', $assign);
});
//系统消息
$app->get('/user/system/msg', function ($request, $response, $args) {
    $signinfo = $this->token->getSignInfo();
    $params = $request->getParams();
    $show = isset($params['page']) ? $params['page'] : 1;
    $list = $this->rest->get("v1/users/system/msg/update?token={$signinfo['token']}&page=".$show);
    $assign['list'] = $list;
    $page = new Page($list->count, $show);
    $assign['page'] = $page;
    $this->renderer->render($response, 'user/system-msg.html', $assign);
});
$app->post('/user/checkuser', function ($request, $response, $args) {
    $params = $request->getParams();
    // echo json_encode($params);
    if($params['type'] == 'tel'){
        $mobile = $this->rest->get("/v1/users/check/mobile?mobile=".$params['user']);
        if(isset($mobile->errno)){
            return json_encode($mobile);
        }
    }else{
        $email = $this->rest->get("/v1/users/check/email?email=".$params['user']);
        if(isset($email->errno)){
            return json_encode($email);
        }
    }
    return json_encode(['data'=>'success']);
});