<?php
//统计直播观看时长
$app->post('/c/{id}', function ($request, $response, $args) {
    $post = $request->getParams();
    $clientinfo = $this->token->getSpectator();
	$ispv = empty($post['id'])? 1 : 0;
	$device = empty($post['device'])? 'other' : $post['device'];
	$app = empty($post['app'])? 'other' : $post['app'];
    $query = ['type' => $post['type'], 'stream_id' => $args['id'],'province'=>$clientinfo['province'],'city'=>$clientinfo['city'], 'client_ip' => $clientinfo['ip'],'device'=>$device,'app'=>$app,'ispv'=>$ispv];
    $data = $this->rest->post('v1/streams/logs', $query);

    return json_encode($data);
});

//获取直播信息
$app->post('/watch/info', function ($request, $response, $args) {
    //频道信息
    $post = $request->getParams();
    
    $streaminfo = $this->kvstorage->get('info'.$post['id']);
    if(empty($streaminfo)){
        $streaminfo = $this->rest->get("v1/streams/{$post['id']}");
        if(isset($streaminfo->errno)){
            $streaminfo->code = 101;
            return json_encode($streaminfo);
        }
        //观看信息
        $endtime = time();
        $count = $this->rest->get("v1/streams/count/{$streaminfo->id}?starttime={$streaminfo->start_live}&endtime={$endtime}&type=pano_live");
        $streaminfo->viewcount = intval($count->watch_total);
        unset($endtime);
        unset($count);
        //自定义信息
        $custom = (array) $this->rest->get("v1/streams/custom/{$streaminfo->id}");
        // $custom['errno'] = 101;
        if (isset($custom['errno'])) {
            $custom = (object)['template'=>'simple','custom_config' => ['downlogo_src'=>"http://yyport.com/static/home/img2/index/1_img_4.png"]];
        }else{
            $custom_config = (object)$custom['custom_config'];
            if((!isset($custom_config->downlogo_src) || $custom_config->downlogo_src =='') && $streaminfo->third_party == 'DETU'){
               $custom_config->downlogo_src = 'http://yyport.com/static/home/img2/index/1_img_4.png';
            }
            $custom['custom_config'] = $custom_config;
        }

        // var_dump($custom);
        $streaminfo->custom = $custom;
        // var_dump($streaminfo);
        unset($count);
        $streaminfo->code = 200;
        $streaminfo = json_encode($streaminfo);
        $this->kvstorage->set('info'.$post['id'], $streaminfo, 5);
    }else{
        $streaminfo = json_encode($streaminfo);
    }
    $streaminfo = json_decode($streaminfo);
    $streaminfo->hls_url = http().substr($streaminfo->hls_url, strpos($streaminfo->hls_url, ':')+1);
    unset($post);
    // var_dump($streaminfo);
    return json_encode($streaminfo);
});

//观众信息
$app->get('/client/ip', function ($request, $response, $args) {
    $clientinfo = $this->token->getChatUserinfo();
	return json_encode($clientinfo);
});

//Insta360 Nano 推流接口
$app->get('/insta360/pushurl/{id}', function ($request, $response, $args) {
	$secretkey = 'fed993d58bef10dcdfc4c533b4dd7376'; //Insta360SecretKey
	$param = $request->getParams();
	if(empty($param['sign'])){
		return json_encode(['code'=>1,'msg'=>'fail','error'=>'请求签名不能为空']);
	}
	if(md5($secretkey.$args['id']) != $param['sign']){
		return json_encode(['code'=>2,'msg'=>'fail','error'=>'签名不正确']);
	}
	
    $stream = $this->rest->get("v1/streams/{$args['id']}");
	if(isset($stream->errno)){
		return json_encode(['code'=>3,'msg'=>'fail','error'=>$stream->error]);
	}
	$rst = ['code'=>0,'msg'=>'success','pushurl'=>$stream->push_url,'pushstate'=>$stream->playing,'playurl'=>"http://{$_SERVER['HTTP_HOST']}/watch/{$stream->id}"];

    return json_encode($rst);
});

//detu 推流接口
$app->get('/detu/pushurl/{id}', function ($request, $response, $args) {
	$secretkey = '4c2cfc351f75b42229463b81d005551b'; //DetuSecretKey
	$param = $request->getParams();
	if(empty($param['sign'])){
		return json_encode(['code'=>1,'msg'=>'fail','error'=>'请求签名不能为空']);
	}
	if(md5($secretkey.$args['id']) != $param['sign']){
		return json_encode(['code'=>2,'msg'=>'fail','error'=>'签名不正确']);
	}
	
    $stream = $this->rest->get("v1/streams/{$args['id']}");
	if(isset($stream->errno)){
		return json_encode(['code'=>3,'msg'=>'fail','error'=>$stream->error]);
	}
	$rst = ['code'=>0,'msg'=>'success','pushurl'=>$stream->push_url,'pushstate'=>$stream->playing,'playurl'=>"http://{$_SERVER['HTTP_HOST']}/watch/{$stream->id}"];

    return json_encode($rst);
});

//观看页面
$app->get('/watch/{id}[/{state}]', function ($request, $response, $args) {
    
    // exit;
    //频道信息
    $streaminfo = $this->rest->get("v1/streams/{$args['id']}");
    if (isset($streaminfo->errno)) {
        $arr['item']='您访问的页面不存在！ - 目睹科技';
        $arr['content']='您访问的页面不存在！';
        $arr['info']='请检查您所输入的网址是否有误。';
        $this->renderer->render($response, 'stream/arrears.html', $arr);

        return;
    }
    //如果余额小于0，提示欠费，用户无法观看直播
    $remain = $this->rest->get("v1/money/status/{$streaminfo->user_id}");
    if ($remain->data <= 0) {
        $arr['item']='您访问的页面暂时无法访问！ - 目睹科技';
        $arr['content']='您访问的页面暂时无法访问！';
        $arr['info']='直播账户余额不足';
        $this->renderer->render($response, 'stream/arrears.html', $arr);
        return;
    }

    //个性化信息
    $custom = (array) $this->rest->get("v1/streams/custom/{$streaminfo->id}");
    if (isset($custom['errno'])) {
        $custom = ['template'=>'simple','custom_config' => []];
    }

    // 获取统计信息
	$endtime = time();
    $data = $this->kvstorage->get('info'.$args['id']);
    if($data){
        $count->watch_total = $data['viewcount'];
    }else{
        $count = $this->rest->get("v1/streams/count/{$streaminfo->id}?starttime={$streaminfo->start_live}&endtime={$endtime}&type=pano_live");
        $count->watch_total = intval($count->watch_total);
    }
    //观众信息
    $clientinfo = $this->token->getChatUserinfo();
	$settings = $this->get('settings');
    if(isMobile()){
		//$this->live->render($response, "{$custom['template']}/mobile.html", $data);
		return $response->withStatus(301)->withHeader('Location', http()."{$settings['player_url']}live.html?watch=".http()."//{$_SERVER['HTTP_HOST']}/watch/{$args['id']}");
    } else {
	    $data = ['item' => $streaminfo,'custom_config' => $custom['custom_config'], 'count' => $count, 'clientinfo' => $clientinfo];
        $data['player'] = http()."{$settings['player_url']}index.html?watch=".http()."//{$_SERVER['HTTP_HOST']}/watch/{$args['id']}";
		$data['starttime_name'] = '直播时间';
		$data['player_url'] = http().$settings['player_url'];
		$this->live->render($response, "{$custom['template']}/index.html", $data);
    }
});

//观看页面
$app->get('/case/{id}', function ($request, $response, $args) {
	$id = $args['id'];
	$vodinfo = $this->rest->get("/v1/cases/{$id}");

    //频道信息
    $streaminfo = (object)['id'=>0,'user_id'=>0,'type'=>'pano','name'=>$vodinfo->id,'alias'=>$vodinfo->name,'announce'=>$vodinfo->announce,'start_live'=>$vodinfo->created_at];
	
	$vodlist = [];

    //个性化信息
    $custom = ['template'=>'simple','custom_config' => []];

    // 获取统计信息
    $count = (object)['watch_total'=>$vodinfo->viewcount];
    //观众信息
    $clientinfo = $this->token->getChatUserinfo();
	
	$settings = $this->get('settings');
	$domain = $settings['restful_baseurl'];
	$domain = str_replace('http://rest.api.','',$domain);
	if(strstr($vodinfo->link,'.mp4') || strstr($vodinfo->link,'.m3u8')){
	   if(isMobile()){
			return $response->withStatus(301)->withHeader('Location', "http:{$settings['case_player_url']}case.html?id={$vodinfo->id}&ref={$domain}");
            
		} else {
			$data = ['item' => $streaminfo,'vodlist'=>$vodlist, 'custom_config' => $custom['custom_config'], 'count' => $count, 'clientinfo' => $clientinfo];
			$data['player'] = "{$settings['case_player_url']}play.html?vod={$vodinfo->link}";
			$data['starttime_name'] = '创建时间';
			$data['player_url'] = $settings['case_player_url'];
			$this->live->render($response, "{$custom['template']}/index.html", $data);
		}
	}else{
		if(isMobile()){
			return $response->withStatus(301)->withHeader('Location', $vodinfo->link);
		} else {
			$data = ['item' => $streaminfo,'vodlist'=>$vodlist, 'custom_config' => $custom['custom_config'], 'count' => $count, 'clientinfo' => $clientinfo];
			$data['player'] = $vodinfo->link;
			$data['starttime_name'] = '创建时间';
			$data['player_url'] = $settings['case_player_url'];
			$this->live->render($response, "{$custom['template']}/index.html", $data);
		}
	}
});

$app->get('/air/{url}',function($request,$response,$args){
    if(strstr($vodinfo->link,'.mp4') || strstr($vodinfo->link,'.m3u8')){
       if(isMobile()){
            return $response->withStatus(301)->withHeader('Location', "http:{$settings['case_player_url']}case.html?id={$vodinfo->id}&ref={$domain}");
            
        } else {
            $data = ['item' => $streaminfo,'vodlist'=>$vodlist, 'custom_config' => $custom['custom_config'], 'count' => $count, 'clientinfo' => $clientinfo];
            $data['player'] = "{$settings['case_player_url']}play.html?vod={$vodinfo->link}";
            $data['starttime_name'] = '创建时间';
            $data['player_url'] = $settings['case_player_url'];
            $this->live->render($response, "{$custom['template']}/index.html", $data);
        }
    }
});