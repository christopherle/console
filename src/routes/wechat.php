<?php
//获取access_token和openid
$app->get('/token', function ($request, $response, $args) {
    $appId = 'wx7eb18d37429be236';
    $appKey = 'fa5b85e4f30692868be25cd8a620f759';
    $params = $request->getParams();
    if(isset($params['code'])){
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appId.'&secret='.$appKey.'&code='.$params['code'].'&grant_type=authorization_code';
        $rst = getCurl($url);
        return $response->withStatus(301)->withHeader('Location', $params['state'].'?access_token='.$rst->access_token.'&openid='.$rst->openid);
    }
});

//微信登陆
$app->map(['GET', 'POST'], '/wechat/login[/{topic}]', function ($request, $response, $args) {
    if($_POST){
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$_POST['access_token'].'&openid='.$_POST['openid'].'&lang=zh_CN';
        $rst = getCurl($url);
        $query = ['openid'=>$_POST['openid'], 'code'=>$_POST['code'], 'mobile'=>$_POST['mobile'], 'realname'=>$rst->nickname];
        $data = $this->rest->post("v1/users/bind/mobile", $query);
        if(isset($data->errno)){
            $assign['post'] = $_POST;
            $assign['issend'] = 1;
            $this->renderer->render($response, 'wechat/bind-mobile.html', $assign);
        }else{
            @$data->userinfo->headimgurl = $rst->headimgurl;
            $this->token->update((array) $data);
            if(empty($args)){
                $this->renderer->render($response, 'wechat/bind-success.html', ['back'=>'/wechat/center']);
                // return $response->withStatus(301)->withHeader('Location', '/wechat/center');
            } else {
                if(substr($args['topic'],0,10) <= time()-600){
                    $assign = ['overdate'=>1];
                }else{
                    $assign = ['topic' => $args['topic'], 'token'=>$data->token, 'uin'=>$data->userinfo->id, "overdate"=>0];
                }
                $this->renderer->render($response, 'wechat/pclogin.html',$assign);  
            }
        }
    }else{
        // $params = $request->getParams();
        // if(empty($params['access_token']) && empty($params['openid'])){
        //     $params = $_SESSION['params'];
        // }else{
        //     $_SESSION['params'] = $params;
        // }
        // if(isset($params['access_token']) && isset($params['openid'])){
        //     //如果获取到access_token和openid，判断openid是不是绑定过手机号
        //     $openid = $params['openid'];
        //     $rst = $this->rest->get("v1/users/wechat/openid?openid=".$openid);
        //     if(isset($rst->errno)){
                $assign = ['access_token'=>$params['access_token'], 'openid'=>$openid];
                $this->renderer->render($response, 'wechat/bind-mobile.html', $assign);
   //          }else{
   //              $url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$params['access_token'].'&openid='.$params['openid'].'&lang=zh_CN';
   //              $WXrst = getCurl($url);
   //              @$rst->userinfo->headimgurl = $WXrst->headimgurl;
   //              $this->token->update((array) $rst);
   //              if(empty($args)){
   //                  return $response->withStatus(301)->withHeader('Location', '/wechat/center');
   //              } else {
   //                  if(substr($args['topic'],0,10) <= time()-600){
   //                      $assign = ['overdate'=>1];
   //                  }else{
   //                      $assign = ['topic' => $args['topic'], 'token'=>$rst->token, 'uin'=>$rst->userinfo->id, "overdate"=>0];
   //                  }
   //                  $this->renderer->render($response, 'wechat/pclogin.html',$assign);   
   //              }
   //          }
   //      }else{
   //          //如果没有接收到access_token和openid
   //          $appId = 'wx7eb18d37429be236';
   //          $appKey = 'fa5b85e4f30692868be25cd8a620f759';
   //          // $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$appId.'&redirect_uri=http%3A%2F%2F'.$_SERVER['HTTP_HOST'].'%2Ftoken&response_type=code&scope=snsapi_userinfo&state=http%3A%2F%2F'.$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"].'#wechat_redirect';
   //           $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$appId.'&redirect_uri=http%3A%2F%2Fmoredoo.com%2Ftoken&response_type=code&scope=snsapi_userinfo&state=http%3A%2F%2F'.$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"].'#wechat_redirect';
			// return $response->withStatus(301)->withHeader('Location', $url);
   //      }
    }
});

//微信个人中心
$app->get('/wechat/center', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $signinfo = $this->token->getSignInfo();
    $userinfo = $signinfo['userinfo'];
    $assign['userinfo'] = $userinfo;
    //获取用户余额
    $balance = $this->rest->get("v1/money/balance/{$userinfo->id}?token={$signinfo['token']}");
    $assign['balance'] = isset($balance->data)? $balance->data : '0.00';
    //获取系统消息
    $assign['msg'] = $this->rest->get('/v1/users/system/msg?token='.$signinfo['token']);
    $assign['signinfo'] = json_encode($signinfo);
    $this->renderer->render($response, 'wechat/center.html', $assign);
});

//我的直播
$app->get('/wechat/stream/list', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $userinfo = $this->token->getSignInfo();
    $list = $this->rest->get("v1/streams?token={$userinfo['token']}");
    $assign = ['list' => $list];
    // var_dump($list);
    $this->renderer->render($response, 'wechat/stream-list.html', $assign);
});

//删除直播
$app->get('/wechat/delete/{id}', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $userinfo = $this->token->getSignInfo();
    $list = $this->rest->delete("v1/streams/{$args['id']}?token={$userinfo['token']}");
    if (isset($list->errno)) {
        throw new Exception($list->error, 101);
    } else {
        return $response->withStatus(301)->withHeader('Location', '/wechat/stream/list');
    }
});

//创建直播
$app->map(['GET', 'POST'], '/wechat/stream/create', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $userinfo = $this->token->getSignInfo();
    $id = $userinfo['userinfo']->id;
    $post = $request->getParams();
    $assign['id'] = $id;
    if (empty($post)) {
        $assign['post'] = ['alias' => '', '', 'start_live' => time(), 'stop_live' => time()+14400];
    } else {
        $post['start_live'] = strtotime($post['start_live']);
        $post['stop_live'] = strtotime($post['stop_live']);
        $assign['post'] = $post;
        $userinfo = $this->token->getSignInfo();
        $post['token'] = $userinfo['token'];
        $post['name'] = $id.substr(md5(time().uniqid()), rand(0, 16), 16 - strlen($id));
        $post['cover'] = "http://{$_SERVER['HTTP_HOST']}/static/console/img/cover.jpg";
        $post['h5_cover'] = "http://{$_SERVER['HTTP_HOST']}/static/console/img/h5_cover.jpg";
        $data = $this->rest->post('v1/streams', $post);
        if (!isset($data->errno)) {
            $item = $this->rest->get("v1/streams/{$data->data}");
            $assign['item'] = (array)$item;
            $this->renderer->render($response, 'wechat/create-success.html', $assign);
            return;
        } else {
            // $assign['error'] = $data->error;
            // $this->renderer->render($response, 'error.html', ['back' => '/stream/my', 'msg' => $data->error]);
            
            // return json_encode(['rst'=>'0','tip'=>$data->error]);
            $assign['post'] = $post;
            $assign['error'] = $data->error;
        }
    }
    
    $this->renderer->render($response, 'wechat/create.html', $assign);
});

//系统消息
$app->get('/wechat/system/msg', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $signinfo = $this->token->getSignInfo();
    $params = $request->getParams();
    $show = isset($params['page']) ? $params['page'] : 1;
    $list = $this->rest->get("v1/users/system/msg/update?token={$signinfo['token']}&page=".$show);
    $assign['list'] = $list;
    $this->renderer->render($response, 'wechat/msg.html', $assign);
});


$app->any('/wechat/notify', function ($request, $response, $args) {
    //ini_set('date.timezone','Asia/Shanghai');
	$xml = $request->getBody();
	$this->rest->post('/v1/logs/add', ['type'=>'wechat','error'=>$xml]);
    require_once __DIR__.'/wxsdk/lib/WxPay.Api.php';
    $data = WxPayResults::Init($xml);
    $putdata = ['token'=>$data['attach'],'orderid'=>$data['out_trade_no'],'descr'=>"微信订单号：{$data['transaction_id']}"];
    $data = $this->rest->put('v1/money/recharge', $putdata);
    $reply = new WxPayNotifyReply();
    if(!isset($data->errno)){
        $reply->SetReturn_code("SUCCESS");
        $reply->SetReturn_msg("OK");
    }else{
        $reply->SetReturn_code("FAIL");
        $reply->SetReturn_msg($data->error);
    }
	
    return $reply->ToXml();
});

$app->get('/wechat/payparameters',function ($request, $response, $args) {
    $signinfo = $this->token->getSignInfo();
	if(empty($signinfo)){
		return json_encode(['code'=>101,'error'=>'fail']);
	}
    require_once __DIR__.'/wxsdk/WxPay.JsApiPay.php';
    $params = $request->getParams();
    $orderid = $signinfo['userinfo']->id . date('YmdHis') . mt_rand(100,999);
    $money = (float)number_format($params['amount'],2,'.','');
    $postdata = ['token'=>$signinfo['token'],'orderid'=>$orderid,'money'=>$money,'type'=>'wxpay'];
    $this->rest->post("v1/money/orderid/{$signinfo['userinfo']->id}", $postdata);
    $tools = new JsApiPay();
    $openId = $params['openid'];
    $input = new WxPayUnifiedOrder();
    $input->SetBody("目睹控制台充值{$money}元");
    $input->SetAttach($signinfo['token']);
    $input->SetOut_trade_no($orderid);
    $input->SetTotal_fee($money*100);
    $input->SetTime_start(date("YmdHis"));
    $input->SetTime_expire(date("YmdHis", time() + 600));
    $input->SetGoods_tag("1");
    $input->SetNotify_url("http://".$_SERVER['HTTP_HOST']."/wechat/notify");
    $input->SetTrade_type("JSAPI");
    $input->SetOpenid($openId);
    $order = WxPayApi::unifiedOrder($input);
    $jsApiParameters = $tools->GetJsApiParameters($order);
    return json_encode($jsApiParameters);
});

//微信充值
$app->get('/wechat/recharge', function ($request, $response, $args) {
    $params = $request->getParams();
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $signinfo = $this->token->getSignInfo();
    $assign['openid'] = $signinfo['userinfo']->openid;
    $this->renderer->render($response, 'wechat/recharge.html',$assign);
});

//操作中心
$app->get('/wechat/operation', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $this->renderer->render($response, 'wechat/operation.html');
});

//微信扫码登陆
$app->get('/wechat/scan/login/{token}/{id}', function ($request, $response, $args) {
    $rst = $this->rest->get("v1/users/{$args['id']}?token={$args['token']}");
    $data = ['token'=>$args['token'], 'userinfo'=>$rst];
    $this->token->update($data);
    return $response->withStatus(301)->withHeader('Location', '/console');
});

//实名认证
$app->map(['GET', 'POST'] ,'/wechat/realname', function ($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $assign = [];
    $signinfo = $this->token->getSignInfo();
    $post = $request->getParams();
    $assign['post'] = $this->rest->get("v1/users/realname/{$signinfo['userinfo']->id}?token={$signinfo['token']}");
    if(!empty($post)){
        $query = ['token'=>$signinfo['token'],'user_id'=>$signinfo['userinfo']->id,'type'=>$post['utype'],'idcode'=>$post['idNumber'],'ida'=>$post['id1'],'idb'=>$post['id2'],'company'=>$post['company'],'company_id'=>$post['businessLicence'],'company_code'=>$post['organization'],'license'=>$post['businessFile'],'status'=>0];
        $data = $this->rest->post("v1/users/realname/{$signinfo['userinfo']->id}", $query);
        if(!isset($data->errno)){
            return $response->withStatus(301)->withHeader('Location', '/wechat/realname');
        }
    }
    $settings = $this->get('settings');
    $assign['uploadurl'] = $settings['storage_baseurl'].'/addimage.php?id='.$signinfo['userinfo']->id.'&token='.$settings['storage_token'];
    $this->renderer->render($response, 'wechat/realname.html',$assign);
});

//重置实名认证
$app->get('/wechat/realname/cancel', function($request, $response, $args) {
    if (empty($_SESSION['sigininfo'])) {
        return $response->withStatus(301)->withHeader('Location', '/wechat/login');
    }
    $signinfo = $this->token->getSignInfo();
    $post = $request->getParams();
    $rst = $this->rest->delete("v1/users/realname/{$post['id']}?token={$signinfo['token']}",$query);
    if(isset($rst->errno)){
        exit('重置失败');
    }else{
        return $response->withStatus(301)->withHeader('Location', '/wechat/realname');
    }
});

//微信扫码支付->扫码
$app->get('/wechat/scan/code', function ($request,$response,$args) {
    $params = $request->getParams();
    $this->token->checkSignStatus();
    $signinfo = $this->token->getSignInfo();
    $money = (float)number_format($params['money'],2,'.','');;
    $orderid = $signinfo['userinfo']->id . date('YmdHis') . mt_rand(100,999);
    $postdata = ['token'=>$signinfo['token'],'orderid'=>$orderid,'money'=>$money,'type'=>'wxpay'];
    $this->rest->post("v1/money/orderid/{$signinfo['userinfo']->id}", $postdata);
    require_once __DIR__."/wxsdk/lib/WxPay.Api.php";
    require_once __DIR__.'/wxsdk/WxPay.NativePay.php';
    $notify = new NativePay();
    $input = new WxPayUnifiedOrder();
    $input->SetBody("目睹控制台充值{$money}元");
    $input->SetAttach($signinfo['token']);
    $input->SetOut_trade_no($orderid);
    $input->SetTotal_fee($money*100);
    $input->SetTime_start(date("YmdHis"));
    $input->SetTime_expire(date("YmdHis", time() + 600));
    $input->SetGoods_tag("1");
    $input->SetNotify_url("http://".$_SERVER['HTTP_HOST']."/wechat/notify");
    $input->SetTrade_type("NATIVE");
    $input->SetProduct_id($orderid);
    $result = $notify->GetPayUrl($input);
    $url = $result["code_url"];
    // var_dump($url1);
    $assign['url'] = urlencode($url);
    $assign['money'] = $money;
    $assign['orderid'] = $orderid;
    $this->renderer->render($response, 'wechat/scan.html', $assign);
});
$app->post('/wechat/scan/pay/status', function ($request, $response, $args) {
    $signinfo = $this->token->getSignInfo();
    $token = $signinfo['token'];
    $post = $request->getParams();
    $rst = $this->rest->get("v1/money/order/status/{$signinfo['userinfo']->id}?orderid={$post['orderid']}&token={$token}");
    if(isset($rst->errno)){
        $data = ['result'=>'FAIL','error'=>'请重新登录'];
    }else{
        $data = ['result'=>'SUCCESS','status'=>$rst->status];
    }
    echo json_encode($data);
});

$app->get('/test/pay',function ($request, $response, $args) {
    require_once __DIR__."/wxsdk/lib/WxPay.Api.php";
    require_once __DIR__.'/wxsdk/WxPay.NativePay.php';
    $notify = new NativePay();
    $url = $notify->GetPrePayUrl("123456789");
    var_dump($url);
    $assign['url'] = $url;
    $this->renderer->render($response, 'wechat/test-pay.html', $assign);
});