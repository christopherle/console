<?php
return [
    'settings' => [
        'displayErrorDetails' => false, // set to false in production

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__.'/../public/themes/console/',
			'liveview_path' => __DIR__.'/../public/themes/liveview/',
			'template_web' => __DIR__.'/../public/themes/home/',
        ],
		'player_url' => '//playhls.moredoo.com/moredooplayer/player/',
        'case_player_url' => '//video.moredoo.com/player/1.71/',
		'restful_baseurl' => 'http://rest.api.moredoo.com',
		'storage_baseurl' => '//static.moredoo.com',
		'storage_token'   => '22e8c8d3be935cb4fd4b7ff5d8985d40',
        'lps_url'      => 'lps.moredoo.com/moredoopano/',
    ],
    'redis' => [
        'host' => '127.0.0.1',
        'port' => 6379,
        'password' => 'kkyoo_aodian_2011_06_11'
    ],
];