#!/bin/bash
mkdir ./src/dependencies
mkdir ./src/middleware
composer update
composer dump-autoload
rm -rf ./src/settings.php
cp ./src/settings_release.php ./src/settings.php
